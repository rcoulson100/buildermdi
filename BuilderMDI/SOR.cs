﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace BuilderMDI
{
    public partial class SOR : Form
    {
        SqlConnection myConnection;
        DataTable dtSelected;
        Form myParent;

        public SOR(Form p, SqlConnection cnn)
        {
            InitializeComponent();
            this.Icon = BuilderMDI.Properties.Resources.ICON;
            myConnection = cnn;
            myParent = p;
        }

        private void SOR_Load(object sender, EventArgs e)
        {
            Form frm = sender as Form;
            frm.Size = new Size(1000, 600);
            SetScreen(frm);
        }

        private void SetScreen(Form frm)
        {
            if (frm.Width <= 600)
                frm.Width = 600;

            groupBox1.Left = 5;
            groupBox1.Top = 5;
            groupBox1.Width = frm.ClientSize.Width - groupBox1.Left - 5;
            groupBox1.Height = frm.ClientSize.Height - groupBox1.Top - btDetail.ClientSize.Height - 10;

            gridData.Left = 5;
            gridData.Top = 12;
            gridData.Width = groupBox1.Width - (gridData.Left * 2);
            gridData.Height = groupBox1.Height - gridData.Top - 10;

            btCreate.Left = 5;
            btCreate.Top = frm.ClientSize.Height - btCreate.ClientSize.Height - 5;
            btEdit.Left = btCreate.Left + btCreate.ClientSize.Width + 10;
            btEdit.Top = btCreate.Top;
            btDelete.Left = btEdit.Left + btEdit.ClientSize.Width + 10;
            btDelete.Top = btCreate.Top;
            btDetail.Left = btDelete.Left + btDelete.ClientSize.Width + 10;
            btDetail.Top = btCreate.Top;
            btClose.Left = btDetail.Left + btDetail.ClientSize.Width + 10; ;
            btClose.Top = btCreate.Top;

        }

        private void SOR_Shown(object sender, EventArgs e)
        {
            PopulateGrid();
            //SetButtonStates();
        }

        public void ForceRefresh()
        {
            PopulateGrid();
        }

        private void PopulateGrid()
        {
            try
            {
                int row = -1;

                if (gridData.SelectedRows.Count == 1)
                    row = gridData.SelectedRows[0].Index;

                SqlCommand command = new SqlCommand("sp_SOR_Select", myConnection);
                command.CommandType = CommandType.StoredProcedure;

                // populate the grid
                SqlDataReader reader = command.ExecuteReader();
                dtSelected = new DataTable();
                dtSelected.Load(reader);

                gridData.DataSource = new DataView(dtSelected);
                SetGridHeader();
                Globals.UpdateGridFont(gridData);

                // re-select the row
                if (row != -1)
                {
                    if (row > gridData.RowCount)
                        row = gridData.RowCount;

                    gridData.CurrentCell = gridData.Rows[row].Cells[1];
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }

        private void SetGridHeader()
        {
            try
            {
                gridData.Columns["SORID"].Visible = false;
                gridData.Columns["Rated"].Visible = false;

                gridData.Columns["SORDesc"].HeaderText = "SOR Description";
                gridData.Columns["SORDesc"].Width = 700;
                gridData.Columns["RatedText"].HeaderText = "Use Rate Bands";
                gridData.Columns["RatedText"].Width = 130;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
            return;
        }

        private void SOR_Resize(object sender, EventArgs e)
        {
            Form frm = sender as Form;
            SetScreen(frm);
        }

        private void SOR_FormClosed(object sender, FormClosedEventArgs e)
        {
            //if (myParent.GetType() == typeof(Main))
            //{
            //    Globals.RestoreScreen(myParent);
            //}
        }

        private void btDetail_Click(object sender, EventArgs e)
        {
            try
            {
                if (gridData.SelectedRows.Count == 0)
                    return;

                var row = gridData.SelectedRows[0].Index;

                int sorID = (int)gridData.Rows[row].Cells["SORID"].Value;

                var sorDetail = new SORDetail(this, myConnection, sorID);
                sorDetail.MdiParent = Globals.MDIParentForm;
                sorDetail.Show();
                Globals.HideScreen(this);
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }


        private void btDelete_Click(object sender, EventArgs e)
        {
            if (DialogResult.No == MessageBox.Show("Are you sure?", "SOR", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                return;

            if (gridData.SelectedRows.Count == 0)
                return;

            var row = gridData.SelectedRows[0].Index;

            int sorID = (int)gridData.Rows[row].Cells["SORID"].Value;

            if (DoDelete(sorID) == 0)
                PopulateGrid();
        }

        private int DoDelete(int sorID)
        {
            int retval = -1;

            try
            {
                // Set uyp the command object
                SqlCommand command = new SqlCommand("sp_SOR_Delete", myConnection);
                command.CommandType = CommandType.StoredProcedure;

                // Add all parmeters including the output parameters here
                command.Parameters.Add(new SqlParameter("@SORID", sorID));

                SqlParameter parRetval = command.Parameters.Add("@retval", SqlDbType.Int);
                parRetval.Direction = ParameterDirection.ReturnValue;

                // Execute the SP
                command.ExecuteNonQuery();

                retval = (int)command.Parameters["@retval"].Value;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
                retval = -1;
            }

            return retval;
        }


        private void btCreate_Click(object sender, EventArgs e)
        {
            SORCreate frm = new SORCreate(this, myConnection, SORCreate.ScreenMode.Create);
            frm.MdiParent = Globals.MDIParentForm;
            frm.Show();
            frm.WindowState = FormWindowState.Normal;
            Globals.HideScreen(this);
        }

        private void btEdit_Click(object sender, EventArgs e)
        {
            try
            {

                var row = gridData.SelectedRows[0].Index;
                int sorID = (int)gridData.Rows[row].Cells["SORID"].Value;
                String sorDesc = gridData.Rows[row].Cells["SORDesc"].Value.ToString();
                String rated = gridData.Rows[row].Cells["RatedText"].Value.ToString();

                SORCreate changePin = new SORCreate(this, myConnection, SORCreate.ScreenMode.Edit, sorID, sorDesc, rated);
                changePin.MdiParent = Globals.MDIParentForm;
                changePin.Show();
                changePin.WindowState = FormWindowState.Normal;
                Globals.HideScreen(this);
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
                return;
            }
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gridData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                PopulateGrid();
            }
        }

    }
}
