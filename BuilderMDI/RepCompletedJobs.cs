﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace BuilderMDI

{
    public partial class RepCompletedJobs : Form
    {
        private bool bFileOpen = false;
        private const string FILETYPEFILTER = "CSV|*.csv";
        private const string FILETYPEDEFAULT = ".csv";
        private string DEFAULTFILELOCATION;
        
        SqlConnection myConnection;
        Form myParent;

        public RepCompletedJobs(Form parent, SqlConnection cnn)
        {
            InitializeComponent();
            this.Icon = BuilderMDI.Properties.Resources.ICON;
            DEFAULTFILELOCATION = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            myConnection = cnn;
            myParent = parent;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String fileName = "CompletedWorkOrders";

            if (cbFileName.Checked == true)
            {
                fileName += "_" + DateTime.Now.Day.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Year.ToString();
            }

            var save = new SaveFileDialog
            {
                Filter = FILETYPEFILTER,
                InitialDirectory = DEFAULTFILELOCATION,
                DefaultExt = FILETYPEDEFAULT,
                FileName = fileName
            };

            if (save.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    // Get data
                    SqlCommand command = new SqlCommand("sp_RepCompletedJobs_Select", myConnection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@startDate", dtStart.Value));
                    command.Parameters.Add(new SqlParameter("@endDate", dtEnd.Value));

                    SqlDataReader reader = command.ExecuteReader();
                    DataTable reportData = new DataTable();
                    reportData.Load(reader);

                 
                    // run the export to file
                    using (StreamWriter writer = new StreamWriter(save.FileName))
                    {
                        CSVFileWriter.WriteDataTable(reportData, writer, true);
                    }

                    // open file after saving?
                    if (cbOpen.Checked == true)
                    {
                        System.Diagnostics.Process.Start(save.FileName);
                    }

                    this.Close();
                }
                catch (Exception err)
                {
                    MessageBox.Show(err.Message);
                }
            }
        }
    }
}
