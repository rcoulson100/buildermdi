﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BuilderMDI
{
    public partial class ChangeJobNumber : Form
    {

        SqlConnection myConnection;
        int WorkID;
        Form myParent;
        String JobNumber;
        bool bRefreshNewJobNumber = false;

        public ChangeJobNumber()
        {
            InitializeComponent();
        }

        public ChangeJobNumber(Form parent, SqlConnection cnn, int workId, String jobNumber)
        {
            InitializeComponent();
            this.Icon = BuilderMDI.Properties.Resources.ICON;
            myConnection = cnn;
            WorkID = workId;
            myParent = parent;
            JobNumber = jobNumber;
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            int retval = -1;

            try
            {
                if (tbJobNumber.Text.Length == 0)
                {
                    MessageBox.Show("The Work Order number must be entered.", "Work Order", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                else
                {
                    long tmpJobNumber = -1;
                    if (!long.TryParse(tbJobNumber.Text, out tmpJobNumber))
                    {
                        MessageBox.Show("The Work Order number must be a number.", "Work Order", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }

                    if (tmpJobNumber <= 0)
                    {
                        MessageBox.Show("The Work Order number must be greater than 0.", "Work Order", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }

                    if (tmpJobNumber > 2147483647)
                    {
                        MessageBox.Show("The Work Order number is too big.", "Work Order", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }

                JobNumber = tbJobNumber.Text;

                retval = ValidateAgainstDB(JobNumber, WorkID);

                // see validate SP for meaning of return values
                if (retval == -1)
                {
                    MessageBox.Show("The Work Order number entered is already in use.", "Work Order", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                // Set up the command object
                SqlCommand command = new SqlCommand("sp_WorkHead_Update_JobNumber", myConnection);
                command.CommandType = CommandType.StoredProcedure;

                // Add all parmeters including the output parameters here
                command.Parameters.Add(new SqlParameter("@WorkID", WorkID));
                command.Parameters.Add(new SqlParameter("@JobNumber", JobNumber));

                SqlParameter parRetval = command.Parameters.Add("@retval", SqlDbType.Int);
                parRetval.Direction = ParameterDirection.ReturnValue;

                // Execute the SP
                command.ExecuteNonQuery();

                retval = (int)command.Parameters["@retval"].Value;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
                retval = -1;
            }

            if (retval == 0)
            {
                //if (myParent.GetType() == typeof(Main))
                //{
                //    var m = myParent as Main;
                //    m.RestoreScreen(true);
                //}

                //myParent.ForceGridRefresh();
                bRefreshNewJobNumber = true;
                this.Close();
            }

        }

        private int ValidateAgainstDB(String jobNumber, int workId)
        {
            try
            {
                // Check work order does not already exists
                SqlCommand command = new SqlCommand("sp_WorkHead_Validate", myConnection);
                command.CommandType = CommandType.StoredProcedure;

                // Add all parmeters including the output parameters here
                command.Parameters.Add(new SqlParameter("@JobNumber", jobNumber));
                command.Parameters.Add(new SqlParameter("@WorkId", workId));

                SqlParameter parRetval = command.Parameters.Add("@retval", SqlDbType.Int);
                parRetval.Direction = ParameterDirection.ReturnValue;

                // Execute the SP
                command.ExecuteNonQuery();

                int retval = (int)command.Parameters["@retval"].Value;
                return retval;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
                return -1;
            }
        }


        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ChangeJobNumber_Load(object sender, EventArgs e)
        {
            tbJobNumber.Text = JobNumber;
        }

        private void ChangeJobNumber_FormClosed(object sender, FormClosedEventArgs e)
        {
            Globals.RestoreScreen(myParent);

            if (myParent.GetType() == typeof(Main))
            {
                var m = myParent as Main;
                m.RestoreScreen(true);
            }
            else if (myParent.GetType() == typeof(WorkOrderDetail))
            {
                Globals.RestoreScreen(myParent);

                var m = myParent as WorkOrderDetail;
                if (bRefreshNewJobNumber)
                    m.ForceGridRefresh(true, JobNumber);
                else
                    m.ForceGridRefresh(true);
            }
            else
            {
                Globals.RestoreScreen(myParent);
            }
        }

    }
}
