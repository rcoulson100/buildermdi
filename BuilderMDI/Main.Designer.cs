﻿namespace BuilderMDI
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gridWorkOrders = new System.Windows.Forms.DataGridView();
            this.btDetail = new System.Windows.Forms.Button();
            this.btStatus = new System.Windows.Forms.Button();
            this.btHistory = new System.Windows.Forms.Button();
            this.btNew = new System.Windows.Forms.Button();
            this.btClose = new System.Windows.Forms.Button();
            this.btDelete = new System.Windows.Forms.Button();
            this.btChangeComment = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridWorkOrders)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.gridWorkOrders);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(795, 324);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Open Work Orders";
            // 
            // gridWorkOrders
            // 
            this.gridWorkOrders.AllowUserToAddRows = false;
            this.gridWorkOrders.AllowUserToDeleteRows = false;
            this.gridWorkOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridWorkOrders.Location = new System.Drawing.Point(12, 19);
            this.gridWorkOrders.MultiSelect = false;
            this.gridWorkOrders.Name = "gridWorkOrders";
            this.gridWorkOrders.ReadOnly = true;
            this.gridWorkOrders.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridWorkOrders.Size = new System.Drawing.Size(769, 292);
            this.gridWorkOrders.TabIndex = 1;
            this.gridWorkOrders.TabStop = false;
            this.gridWorkOrders.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.gridWorkOrders_CellMouseClick);
            this.gridWorkOrders.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.gridWorkOrders_CellMouseDoubleClick);
            this.gridWorkOrders.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridWorkOrders_KeyDown);
            // 
            // btDetail
            // 
            this.btDetail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btDetail.Location = new System.Drawing.Point(133, 342);
            this.btDetail.Name = "btDetail";
            this.btDetail.Size = new System.Drawing.Size(115, 23);
            this.btDetail.TabIndex = 1;
            this.btDetail.Text = "Details";
            this.btDetail.UseVisualStyleBackColor = true;
            this.btDetail.Click += new System.EventHandler(this.btDetail_Click);
            // 
            // btStatus
            // 
            this.btStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btStatus.Location = new System.Drawing.Point(254, 342);
            this.btStatus.Name = "btStatus";
            this.btStatus.Size = new System.Drawing.Size(115, 23);
            this.btStatus.TabIndex = 2;
            this.btStatus.Text = "Change Status";
            this.btStatus.UseVisualStyleBackColor = true;
            this.btStatus.Click += new System.EventHandler(this.btStatus_Click);
            // 
            // btHistory
            // 
            this.btHistory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btHistory.Location = new System.Drawing.Point(632, 342);
            this.btHistory.Name = "btHistory";
            this.btHistory.Size = new System.Drawing.Size(115, 23);
            this.btHistory.TabIndex = 5;
            this.btHistory.Text = "History";
            this.btHistory.UseVisualStyleBackColor = true;
            this.btHistory.Click += new System.EventHandler(this.btHistory_Click);
            // 
            // btNew
            // 
            this.btNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btNew.Location = new System.Drawing.Point(12, 342);
            this.btNew.Name = "btNew";
            this.btNew.Size = new System.Drawing.Size(115, 23);
            this.btNew.TabIndex = 6;
            this.btNew.Text = "New";
            this.btNew.UseVisualStyleBackColor = true;
            this.btNew.Click += new System.EventHandler(this.btNew_Click);
            // 
            // btClose
            // 
            this.btClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btClose.Location = new System.Drawing.Point(753, 342);
            this.btClose.Name = "btClose";
            this.btClose.Size = new System.Drawing.Size(115, 23);
            this.btClose.TabIndex = 44;
            this.btClose.Text = "Close";
            this.btClose.UseVisualStyleBackColor = true;
            this.btClose.Click += new System.EventHandler(this.btClose_Click);
            // 
            // btDelete
            // 
            this.btDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btDelete.Location = new System.Drawing.Point(511, 342);
            this.btDelete.Name = "btDelete";
            this.btDelete.Size = new System.Drawing.Size(115, 23);
            this.btDelete.TabIndex = 45;
            this.btDelete.Text = "Delete";
            this.btDelete.UseVisualStyleBackColor = true;
            this.btDelete.Click += new System.EventHandler(this.btDelete_Click);
            // 
            // btChangeComment
            // 
            this.btChangeComment.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btChangeComment.Location = new System.Drawing.Point(375, 342);
            this.btChangeComment.Name = "btChangeComment";
            this.btChangeComment.Size = new System.Drawing.Size(131, 23);
            this.btChangeComment.TabIndex = 46;
            this.btChangeComment.Text = "Change Comment";
            this.btChangeComment.UseVisualStyleBackColor = true;
            this.btChangeComment.Click += new System.EventHandler(this.btChangeComment_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1198, 567);
            this.Controls.Add(this.btChangeComment);
            this.Controls.Add(this.btDelete);
            this.Controls.Add(this.btClose);
            this.Controls.Add(this.btNew);
            this.Controls.Add(this.btHistory);
            this.Controls.Add(this.btStatus);
            this.Controls.Add(this.btDetail);
            this.Controls.Add(this.groupBox1);
            this.Name = "Main";
            this.Text = "Kinetic Work Orders";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Main_FormClosed);
            this.Load += new System.EventHandler(this.Main_Load);
            this.Shown += new System.EventHandler(this.Main_Shown);
            this.Resize += new System.EventHandler(this.Main_Resize);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridWorkOrders)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView gridWorkOrders;
        private System.Windows.Forms.Button btDetail;
        private System.Windows.Forms.Button btStatus;
        private System.Windows.Forms.Button btHistory;
        private System.Windows.Forms.Button btNew;
        private System.Windows.Forms.Button btClose;
        private System.Windows.Forms.Button btDelete;
        private System.Windows.Forms.Button btChangeComment;

    }
}