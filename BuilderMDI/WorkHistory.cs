﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BuilderMDI
{
    public partial class WorkHistory : Form
    {

        SqlConnection myConnection;
        int WorkID;
        Form myParent;

        public WorkHistory(Form p, SqlConnection cnn, int workId)
        {
            InitializeComponent();
            this.Icon = BuilderMDI.Properties.Resources.ICON;
            myConnection = cnn;
            WorkID = workId;
            myParent = p;
        }

        private void WorkHistory_Load(object sender, EventArgs e)
        {
            Form frm = sender as Form;
            SetScreen(frm);
        }

        private void SetScreen(Form frm)
        {
            groupBox1.Left = 5;
            groupBox1.Top = 5;
            groupBox1.Width = frm.ClientSize.Width - groupBox1.Left - 5;
            groupBox1.Height = frm.ClientSize.Height - groupBox1.Top - btClose.ClientSize.Height - 10;

            gridWorkOrders.Width = groupBox1.Width - (gridWorkOrders.Left * 2);
            gridWorkOrders.Height = groupBox1.Height - gridWorkOrders.Top - 10;

            btClose.Left = 5;
            btClose.Top = frm.ClientSize.Height - btClose.ClientSize.Height - 5;
       }

        private void WorkHistory_Shown(object sender, EventArgs e)
        {
            PopulateWorkHeadGrid();
        }

        private void PopulateWorkHeadGrid()
        {

            try
            {
                int row = -1;

                if (gridWorkOrders.SelectedRows.Count == 1)
                    row = gridWorkOrders.SelectedRows[0].Index;

                SqlCommand command = new SqlCommand("sp_WorkHeadHistory_Select", myConnection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@WorkId", WorkID));

                // populate the grid
                SqlDataReader reader = command.ExecuteReader();
                DataTable gridData = new DataTable();
                gridData.Load(reader);

                gridWorkOrders.DataSource = new DataView(gridData);
                SetGridHeader();
                Globals.UpdateGridFont(gridWorkOrders);

                // re-select the row
                if (row != -1)
                {
                    if (row > gridWorkOrders.RowCount)
                        row = gridWorkOrders.RowCount;

                    gridWorkOrders.CurrentCell = gridWorkOrders.Rows[row].Cells[1];
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }

        private void SetGridHeader()
        {
            try
            {
                gridWorkOrders.Columns["WorkID"].Visible = false;
                gridWorkOrders.Columns["NewStatusID"].Visible = false;

                gridWorkOrders.Columns["HistoryDate"].HeaderText = "History Date";
                gridWorkOrders.Columns["HistoryDate"].Width = 175;
                gridWorkOrders.Columns["StatusDesc"].HeaderText = "New Status";
                gridWorkOrders.Columns["StatusDesc"].Width = 150;
                gridWorkOrders.Columns["StatusDate"].HeaderText = "Status Changed On";
                gridWorkOrders.Columns["StatusDate"].Width = 175;
                gridWorkOrders.Columns["NewComments"].HeaderText = "Comments Changed To";
                gridWorkOrders.Columns["NewComments"].Width = 400;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
            return;
        }

        private void WorkHistory_Resize(object sender, EventArgs e)
        {
            Form frm = sender as Form;
            SetScreen(frm);
        }

        private void WorkHistory_FormClosed(object sender, FormClosedEventArgs e)
        {
            //if (myParent.GetType() == typeof(Main))
            //{
            //    var m = myParent as Main;
            //    m.RestoreScreen(true);
            //}
            //else if (myParent.GetType() == typeof(WorkOrderDetail))
            //{
                Globals.RestoreScreen(myParent);
            //}
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gridWorkOrders_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                PopulateWorkHeadGrid();
            }
        }
    }
}
