﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BuilderMDI
{
    public partial class Login : Form
    {
        Boolean bCorrectPin = false;
        int loginInAttempts = 0;
        SqlConnection myConnection;

        public Login(SqlConnection cnn)
        {
            InitializeComponent();
            myConnection = cnn;
        }

        private void Login_Load(object sender, EventArgs e)
        {
            this.Icon = BuilderMDI.Properties.Resources.ICON;
        }

        protected override void OnShown(EventArgs e)
        {
            tbUser.Focus();
            base.OnShown(e);
        }

        private void DoLogin()
        {
            int pin = -1;
            if (!int.TryParse(tbPin.Text, out pin))
            {
                MessageBox.Show("PIN must be numeric", "Kinetic Work Orders", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (tbUser.Text.Length == 0)
            {
                MessageBox.Show("You must enter a User Name", "Kinetic Work Orders", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            try
            {
                // Test login
                SqlCommand command = new SqlCommand("sp_User_CheckPin", myConnection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@Name", tbUser.Text));
                command.Parameters.Add(new SqlParameter("@Pin", pin));

                SqlParameter retval = command.Parameters.Add("@retval", SqlDbType.Int);
                retval.Direction = ParameterDirection.ReturnValue;
                command.ExecuteNonQuery();
                int r = (int)command.Parameters["@retval"].Value;

                // If not success write error and exit
                if (r != 0)
                {
                    MessageBox.Show("User name or PIN is incorrect", "Kinetic Work Orders", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    tbPin.Text = "";

                    loginInAttempts++;
                    if (loginInAttempts >= 3)
                    {
                        //Application.Exit();
                        bCorrectPin = false;
                        this.Close();
                    }
                }
                else
                {
                    // Just close this modal dialog and the home screen will open
                    Globals.UserName = tbUser.Text.TrimEnd();
                    bCorrectPin = true;
                    this.Close();
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
                //Application.Exit();
                bCorrectPin = true;
                this.Close();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DoLogin();
        }

        private void Login_FormClosed(object sender, FormClosedEventArgs e)
        {
            // If this form is closing but the pin is not correct them 
            // we must termninal the application
            if (!bCorrectPin)
                Application.Exit();
        }

        private void tbPin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
            {
                DoLogin();
            }
        }
    }
}
