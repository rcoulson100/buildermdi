﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;

namespace BuilderMDI
{
    public partial class Main : Form
    {
        SqlConnection myConnection;
        DataTable gridData;
        Form myParent; 

        public Main(Form p, SqlConnection cnn)
        {
            InitializeComponent();
            this.Icon = BuilderMDI.Properties.Resources.ICON;
            myConnection = cnn;
            myParent = p;
        }

        private void Main_Load(object sender, EventArgs e)
        {
            Form frm = sender as Form;
            frm.Size = new Size(1400, 700);
            SetScreen(frm);
        }

        private void SetScreen(Form frm)
        {
            if (frm.Width <= 900)
                frm.Width = 900;

            groupBox1.Left = 5;
            groupBox1.Top = 5;
            groupBox1.Width = frm.ClientSize.Width - groupBox1.Left - 5;
            groupBox1.Height = frm.ClientSize.Height - groupBox1.Top - btDetail.ClientSize.Height - 10;

            btNew.Top = frm.ClientSize.Height - btNew.ClientSize.Height - 5;
            btDetail.Top = btNew.Top;
            btStatus.Top = btNew.Top;
            btChangeComment.Top = btNew.Top;
            btDelete.Top = btNew.Top;
            btHistory.Top = btNew.Top;
            btClose.Top = btNew.Top;

            btNew.Left = 5;
            btDetail.Left = btNew.Left + btNew.ClientSize.Width + 10;
            btStatus.Left = btDetail.Left + btDetail.ClientSize.Width + 10;
            btChangeComment.Left = btStatus.Left + btStatus.ClientSize.Width + 10;
            btDelete.Left = btChangeComment.Left + btChangeComment.ClientSize.Width + 10;
            btHistory.Left = btDelete.Left + btDelete.ClientSize.Width + 10;
            btClose.Left = btHistory.Left + btHistory.ClientSize.Width + 10;

            gridWorkOrders.Width = groupBox1.Width - (gridWorkOrders.Left * 2);
            gridWorkOrders.Height = groupBox1.Height - gridWorkOrders.Top - 10;
        }

        private void Main_Shown(object sender, EventArgs e)
        {
            PopulateWorkHeadGrid();
            SetButtonStates();
            ////SqlCommand command = new SqlCommand("sp_WorkHead_Select", myConnection);
            ////command.CommandType = CommandType.StoredProcedure;
            ////command.Parameters.Add(new SqlParameter("@AllOpen",  1));

            ////// populate the grid
            ////SqlDataReader reader = command.ExecuteReader();
            ////DataTable gridData = new DataTable();
            ////gridData.Load(reader);

            ////gridWorkOrders.DataSource = new DataView(gridData);
        }


        //public void ForceGridRefresh()
        //{
        //    PopulateWorkHeadGrid();
        //    SetButtonStates();
        //}
        public void RestoreScreen(Boolean refresh)
        {
            //this.Enabled = true;
            //this.WindowState = FormWindowState.Normal;
            PopulateWorkHeadGrid();
            SetButtonStates();
        }

        private void PopulateWorkHeadGrid()
        {
            int row = -1;
            try
            {
                // if a record was selected then try to re-select after populate
                if (gridWorkOrders.SelectedRows.Count == 1)
                    row = gridWorkOrders.SelectedRows[0].Index;

                SqlCommand command = new SqlCommand("sp_WorkHead_Select", myConnection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@AllOpen", 1));

                // populate the grid
                SqlDataReader reader = command.ExecuteReader();
                gridData = new DataTable();
                gridData.Load(reader);

                gridWorkOrders.DataSource = new DataView(gridData);
                SetGridHeader();
                Globals.UpdateGridFont(gridWorkOrders);

                // re-select the row
                if (row != -1)
                {
                    if (row > gridWorkOrders.RowCount - 1)
                        row = gridWorkOrders.RowCount - 1;

                    gridWorkOrders.CurrentCell = gridWorkOrders.Rows[row].Cells[1];
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }

        }

        private void SetGridHeader()
        {
            try
            {
                gridWorkOrders.Columns["WorkID"].Visible = false;
                gridWorkOrders.Columns["CustomerID"].Visible = false;
                gridWorkOrders.Columns["StatusID"].Visible = false;
                gridWorkOrders.Columns["ClerkID"].Visible = false;
                gridWorkOrders.Columns["Address4"].Visible = false;
                gridWorkOrders.Columns["Address5"].Visible = false;

                gridWorkOrders.Columns["WorkID"].HeaderText = "Work ID";

                gridWorkOrders.Columns["JobNumber"].HeaderText =  "Work Order";
                gridWorkOrders.Columns["DateRaised"].HeaderText = "Date Raised";
                gridWorkOrders.Columns["DateIssued"].HeaderText = "Date Issued";
                gridWorkOrders.Columns["ClerkName"].HeaderText = "Clerk";
                gridWorkOrders.Columns["PriorityDesc"].HeaderText = "Priority";
                gridWorkOrders.Columns["StatusDesc"].HeaderText = "Status";
                gridWorkOrders.Columns["StatusDate"].HeaderText = "Status Date";
                gridWorkOrders.Columns["Comments"].HeaderText = "Comments";
                gridWorkOrders.Columns["DateOnSite"].HeaderText = "Date On Site";
                gridWorkOrders.Columns["DateCompleted"].HeaderText = "Date Completed";
                gridWorkOrders.Columns["CustomerID"].HeaderText = "Customer ID";
                gridWorkOrders.Columns["CustomerName"].HeaderText = "Name";
                gridWorkOrders.Columns["Address1"].HeaderText = "Address Line 1";
                gridWorkOrders.Columns["Address2"].HeaderText = "Address Line 2";
                gridWorkOrders.Columns["Address3"].HeaderText = "Address Line 3";
                gridWorkOrders.Columns["PostCode"].HeaderText = "Post Code";
                gridWorkOrders.Columns["TelNo"].HeaderText = "Tel No.";

                gridWorkOrders.Columns["SurveyedUnitTotal"].HeaderText = "Surveyed (£)";
                gridWorkOrders.Columns["ActualUnitTotal"].HeaderText = "Actual (£)";

                gridWorkOrders.Columns["JobNumber"].Width = 110;
                gridWorkOrders.Columns["DateRaised"].Width = 110;
                gridWorkOrders.Columns["DateIssued"].Width = 110;
                gridWorkOrders.Columns["ClerkName"].Width = 110;
                gridWorkOrders.Columns["PriorityDesc"].Width = 90;
                gridWorkOrders.Columns["StatusDesc"].Width = 90;
                gridWorkOrders.Columns["StatusDate"].Width = 110;
                gridWorkOrders.Columns["Comments"].Width = 200;
                gridWorkOrders.Columns["DateOnSite"].Width = 120;
                gridWorkOrders.Columns["DateCompleted"].Width = 120;
                gridWorkOrders.Columns["CustomerName"].Width = 150;
                gridWorkOrders.Columns["Address1"].Width = 150;
                gridWorkOrders.Columns["Address2"].Width = 150;
                gridWorkOrders.Columns["Address3"].Width = 150;
                gridWorkOrders.Columns["PostCode"].Width = 90;
                gridWorkOrders.Columns["TelNo"].Width = 100;
                gridWorkOrders.Columns["SurveyedUnitTotal"].Width = 100;
                gridWorkOrders.Columns["ActualUnitTotal"].Width = 100;

            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
            return;
        }

        private void SetButtonStates()
        {
            int statusId = -1; 
            if (gridWorkOrders.SelectedRows.Count == 1)
            {
                var row = gridWorkOrders.SelectedRows[0].Index;
                string strStatusID = gridWorkOrders.Rows[row].Cells["StatusID"].Value.ToString();
                if (!int.TryParse(strStatusID, out statusId))
                    statusId = -1;
            }

            btDetail.Enabled = (gridWorkOrders.SelectedRows.Count > 0);
            btHistory.Enabled = (gridWorkOrders.SelectedRows.Count > 0);
            btStatus.Enabled = (gridWorkOrders.SelectedRows.Count > 0);
            btChangeComment.Enabled = (gridWorkOrders.SelectedRows.Count > 0);
            btDelete.Enabled = (gridWorkOrders.SelectedRows.Count > 0) && (statusId == 10);  // received
            return;
        }

        private void Main_Resize(object sender, EventArgs e)
        {
            Form frm = sender as Form;
            SetScreen(frm);
        }

        private void Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            gridData.Dispose();
        }

        private void btDetail_Click(object sender, EventArgs e)
        {
            if (gridWorkOrders.SelectedRows.Count == 0)
                return;

            var row = gridWorkOrders.SelectedRows[0].Index;

            string jobNumber = gridWorkOrders.Rows[row].Cells["JobNumber"].Value.ToString();

            var wod = new WorkOrderDetail(this, myConnection, ScreenMode.Display, jobNumber);
            wod.MdiParent = Globals.MDIParentForm ;
            wod.Show();
            Globals.HideScreen(this);
        }

        //private void btSearch_Click(object sender, EventArgs e)
        //{
        //    //var wod = new WorkOrderDetail(this, myConnection, ScreenMode.Search, null);
        //    //wod.Show(this);
        //}

        private void btStatus_Click(object sender, EventArgs e)
        {
            if (gridWorkOrders.SelectedRows.Count == 0)
                return;

            var row = gridWorkOrders.SelectedRows[0].Index;

            string strWorkId = gridWorkOrders.Rows[row].Cells["WorkID"].Value.ToString();
            int workId = 0;
            if (!int.TryParse(strWorkId, out workId))
            {
                MessageBox.Show("Failed to get Work ID from the grid");
                return;
            }

            string statusDesc = gridWorkOrders.Rows[row].Cells["StatusDesc"].Value.ToString();
            string comments = gridWorkOrders.Rows[row].Cells["Comments"].Value.ToString();

            var cs = new ChangeStatus(this, myConnection, workId, statusDesc, comments);
            cs.MdiParent = Globals.MDIParentForm;
            cs.Show();
            Globals.HideScreen(this);
            cs.WindowState = FormWindowState.Normal;
        }

        private void gridWorkOrders_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.KeyCode == Keys.F5) || (e.KeyCode == Keys.Escape))
            {
                PopulateWorkHeadGrid();
            }
        }

        private void btHistory_Click(object sender, EventArgs e)
        {
            if (gridWorkOrders.SelectedRows.Count == 0)
                return;

            var row = gridWorkOrders.SelectedRows[0].Index;

            string strWorkId = gridWorkOrders.Rows[row].Cells["WorkID"].Value.ToString();
            int workId = 0;
            if (!int.TryParse(strWorkId, out workId))
            {
                MessageBox.Show("Failed to get Work ID from the grid");
                return;
            }

            var history = new WorkHistory(this, myConnection, workId);
            history.MdiParent = Globals.MDIParentForm;
            history.Show();
            Globals.HideScreen(this);

        }

        private void gridWorkOrders_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex < 0)
                return;

            // Apply Filter
            try
            {
                SetGridHeader();

                DataGridView data = sender as DataGridView;

                String cellName = data.Columns[e.ColumnIndex].Name.ToString();

                var type = data.Columns[e.ColumnIndex].ValueType;
                string filter = String.Empty;

                if (type == typeof(DateTime))
                {
                    DateTime cellData = (DateTime)data.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
                    filter = string.Format(
                        CultureInfo.InvariantCulture, 
                        "{0} >= #{1:MM/dd/yyyy}# AND {2} < #{3:MM/dd/yyyy}#", 
                        cellName, 
                        cellData, 
                        cellName, 
                        cellData.AddDays(1));
                }
                else
                {
                    string cellData = data.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
                    filter = cellName + " = '" + cellData + "'";
                }
                gridData.DefaultView.RowFilter = filter;

                gridWorkOrders.DataSource = gridData;
                gridWorkOrders.Columns[cellName].HeaderText = "{" + gridWorkOrders.Columns[cellName].HeaderText + "}";
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }

        private void btNew_Click(object sender, EventArgs e)
        {
            var wod = new WorkOrderDetail(this, myConnection, ScreenMode.New, null);
            wod.MdiParent = Globals.MDIParentForm;
            wod.Show();
            Globals.HideScreen(this);
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btDelete_Click(object sender, EventArgs e)
        {
            if (gridWorkOrders.SelectedRows.Count == 0)
                return;

            DialogResult retval = MessageBox.Show("Are you sure you want to delete this work order? This can not be undone!", "Work Order", MessageBoxButtons.YesNo);
            if (retval == DialogResult.Yes)
            {
                var row = gridWorkOrders.SelectedRows[0].Index;

                string strWorkId = gridWorkOrders.Rows[row].Cells["WorkID"].Value.ToString();
                int workId = 0;
                if (!int.TryParse(strWorkId, out workId))
                {
                    MessageBox.Show("Failed to get Work ID from the grid");
                    return;
                }

                // Perform the delete
                DeleteWorkOrder(workId);

                // populate the grid
                PopulateWorkHeadGrid();
            }
        }

        private int DeleteWorkOrder(int WorkID)
        {
            int retval = -1;

            try
            {
                // Set uyp the command object
                SqlCommand command = new SqlCommand("sp_WorkHead_Delete", myConnection);
                command.CommandType = CommandType.StoredProcedure;

                // Add all parmeters including the output parameters here
                command.Parameters.Add(new SqlParameter("@WorkID", WorkID));

                SqlParameter parRetval = command.Parameters.Add("@retval", SqlDbType.Int);
                parRetval.Direction = ParameterDirection.ReturnValue;

                // Execute the SP
                command.ExecuteNonQuery();

                retval = (int)command.Parameters["@retval"].Value;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
                retval = -1;
            }

            return retval;
        }

        private void gridWorkOrders_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            SetButtonStates();
        }

        private void btChangeComment_Click(object sender, EventArgs e)
        {
            if (gridWorkOrders.SelectedRows.Count == 0)
                return;

            var row = gridWorkOrders.SelectedRows[0].Index;

            string strWorkId = gridWorkOrders.Rows[row].Cells["WorkID"].Value.ToString();
            int workId = 0;
            if (!int.TryParse(strWorkId, out workId))
            {
                MessageBox.Show("Failed to get Work ID from the grid");
                return;
            }

            string comments = gridWorkOrders.Rows[row].Cells["Comments"].Value.ToString();

            var cs = new ChangeComment(this, myConnection, workId, comments);
            cs.MdiParent = Globals.MDIParentForm;
            cs.Show();
            Globals.HideScreen(this);
            cs.WindowState = FormWindowState.Normal;
        }


        //protected override Size DefaultSize
        //{
        //    get
        //    {
        //        // Set the default size of 
        //        // the form to 500 pixels square. 
        //        return new Size(oldSize.Width, oldSize.Height);
        //    }
        //}
    }
}
