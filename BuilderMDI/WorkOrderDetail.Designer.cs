﻿namespace BuilderMDI
{
    partial class WorkOrderDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btSearchWO = new System.Windows.Forms.Button();
            this.txWorkID = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btEditJobNumber = new System.Windows.Forms.Button();
            this.tbDateCompleted = new System.Windows.Forms.TextBox();
            this.tbDateOnSite = new System.Windows.Forms.TextBox();
            this.btEditDateCompleted = new System.Windows.Forms.Button();
            this.lblDateCompleted = new System.Windows.Forms.Label();
            this.btEditDateOnSite = new System.Windows.Forms.Button();
            this.lblDateOnSite = new System.Windows.Forms.Label();
            this.btEditPriority = new System.Windows.Forms.Button();
            this.btEditClerk = new System.Windows.Forms.Button();
            this.btEditDateIssued = new System.Windows.Forms.Button();
            this.btEditDateRaised = new System.Windows.Forms.Button();
            this.cbClerk = new System.Windows.Forms.ComboBox();
            this.cbPriority = new System.Windows.Forms.ComboBox();
            this.dtDateIssued = new System.Windows.Forms.DateTimePicker();
            this.dtDateRaised = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbJobNumber = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btEditCustomer = new System.Windows.Forms.Button();
            this.tbTelNo = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tbPostCode = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tbAddress3 = new System.Windows.Forms.TextBox();
            this.tbAddress2 = new System.Windows.Forms.TextBox();
            this.tbAddress1 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tbCustomerName = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.gridWorkDetail = new System.Windows.Forms.DataGridView();
            this.btSave = new System.Windows.Forms.Button();
            this.btNew = new System.Windows.Forms.Button();
            this.btDelete = new System.Windows.Forms.Button();
            this.btAmend = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btEditComment = new System.Windows.Forms.Button();
            this.btStatus = new System.Windows.Forms.Button();
            this.tbComments = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.dtStatusDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.tbStatus = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btHistory = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.gridWorkTotals = new System.Windows.Forms.DataGridView();
            this.btClose = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridWorkDetail)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridWorkTotals)).BeginInit();
            this.SuspendLayout();
            // 
            // btSearchWO
            // 
            this.btSearchWO.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btSearchWO.Location = new System.Drawing.Point(118, 12);
            this.btSearchWO.Name = "btSearchWO";
            this.btSearchWO.Size = new System.Drawing.Size(89, 23);
            this.btSearchWO.TabIndex = 1;
            this.btSearchWO.Text = "Search W/O";
            this.btSearchWO.UseVisualStyleBackColor = true;
            this.btSearchWO.Click += new System.EventHandler(this.btSearchWO_Click);
            // 
            // txWorkID
            // 
            this.txWorkID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txWorkID.Location = new System.Drawing.Point(12, 12);
            this.txWorkID.Name = "txWorkID";
            this.txWorkID.Size = new System.Drawing.Size(100, 22);
            this.txWorkID.TabIndex = 0;
            this.txWorkID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txWorkID_KeyDown);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btEditJobNumber);
            this.groupBox1.Controls.Add(this.tbDateCompleted);
            this.groupBox1.Controls.Add(this.tbDateOnSite);
            this.groupBox1.Controls.Add(this.btEditDateCompleted);
            this.groupBox1.Controls.Add(this.lblDateCompleted);
            this.groupBox1.Controls.Add(this.btEditDateOnSite);
            this.groupBox1.Controls.Add(this.lblDateOnSite);
            this.groupBox1.Controls.Add(this.btEditPriority);
            this.groupBox1.Controls.Add(this.btEditClerk);
            this.groupBox1.Controls.Add(this.btEditDateIssued);
            this.groupBox1.Controls.Add(this.btEditDateRaised);
            this.groupBox1.Controls.Add(this.cbClerk);
            this.groupBox1.Controls.Add(this.cbPriority);
            this.groupBox1.Controls.Add(this.dtDateIssued);
            this.groupBox1.Controls.Add(this.dtDateRaised);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.tbJobNumber);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 41);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(392, 207);
            this.groupBox1.TabIndex = 35;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Work Order Details";
            // 
            // btEditJobNumber
            // 
            this.btEditJobNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEditJobNumber.Location = new System.Drawing.Point(319, 16);
            this.btEditJobNumber.Name = "btEditJobNumber";
            this.btEditJobNumber.Size = new System.Drawing.Size(39, 23);
            this.btEditJobNumber.TabIndex = 54;
            this.btEditJobNumber.Text = "Edit";
            this.btEditJobNumber.UseVisualStyleBackColor = true;
            this.btEditJobNumber.Click += new System.EventHandler(this.btEditJobNumber_Click);
            // 
            // tbDateCompleted
            // 
            this.tbDateCompleted.Location = new System.Drawing.Point(126, 177);
            this.tbDateCompleted.Name = "tbDateCompleted";
            this.tbDateCompleted.Size = new System.Drawing.Size(187, 22);
            this.tbDateCompleted.TabIndex = 53;
            // 
            // tbDateOnSite
            // 
            this.tbDateOnSite.Location = new System.Drawing.Point(126, 152);
            this.tbDateOnSite.Name = "tbDateOnSite";
            this.tbDateOnSite.Size = new System.Drawing.Size(187, 22);
            this.tbDateOnSite.TabIndex = 52;
            // 
            // btEditDateCompleted
            // 
            this.btEditDateCompleted.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEditDateCompleted.Location = new System.Drawing.Point(319, 176);
            this.btEditDateCompleted.Name = "btEditDateCompleted";
            this.btEditDateCompleted.Size = new System.Drawing.Size(39, 23);
            this.btEditDateCompleted.TabIndex = 51;
            this.btEditDateCompleted.Text = "Edit";
            this.btEditDateCompleted.UseVisualStyleBackColor = true;
            this.btEditDateCompleted.Click += new System.EventHandler(this.btEditDateCompleted_Click);
            // 
            // lblDateCompleted
            // 
            this.lblDateCompleted.AutoSize = true;
            this.lblDateCompleted.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDateCompleted.Location = new System.Drawing.Point(14, 179);
            this.lblDateCompleted.Name = "lblDateCompleted";
            this.lblDateCompleted.Size = new System.Drawing.Size(106, 16);
            this.lblDateCompleted.TabIndex = 50;
            this.lblDateCompleted.Text = "Date Completed";
            // 
            // btEditDateOnSite
            // 
            this.btEditDateOnSite.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEditDateOnSite.Location = new System.Drawing.Point(319, 151);
            this.btEditDateOnSite.Name = "btEditDateOnSite";
            this.btEditDateOnSite.Size = new System.Drawing.Size(39, 23);
            this.btEditDateOnSite.TabIndex = 48;
            this.btEditDateOnSite.Text = "Edit";
            this.btEditDateOnSite.UseVisualStyleBackColor = true;
            this.btEditDateOnSite.Click += new System.EventHandler(this.btEditDateOnSite_Click);
            // 
            // lblDateOnSite
            // 
            this.lblDateOnSite.AutoSize = true;
            this.lblDateOnSite.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDateOnSite.Location = new System.Drawing.Point(14, 154);
            this.lblDateOnSite.Name = "lblDateOnSite";
            this.lblDateOnSite.Size = new System.Drawing.Size(83, 16);
            this.lblDateOnSite.TabIndex = 47;
            this.lblDateOnSite.Text = "Date On Site";
            // 
            // btEditPriority
            // 
            this.btEditPriority.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEditPriority.Location = new System.Drawing.Point(319, 123);
            this.btEditPriority.Name = "btEditPriority";
            this.btEditPriority.Size = new System.Drawing.Size(39, 23);
            this.btEditPriority.TabIndex = 45;
            this.btEditPriority.Text = "Edit";
            this.btEditPriority.UseVisualStyleBackColor = true;
            this.btEditPriority.Click += new System.EventHandler(this.btEditPriority_Click);
            // 
            // btEditClerk
            // 
            this.btEditClerk.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEditClerk.Location = new System.Drawing.Point(319, 95);
            this.btEditClerk.Name = "btEditClerk";
            this.btEditClerk.Size = new System.Drawing.Size(39, 23);
            this.btEditClerk.TabIndex = 44;
            this.btEditClerk.Text = "Edit";
            this.btEditClerk.UseVisualStyleBackColor = true;
            this.btEditClerk.Click += new System.EventHandler(this.btEditClerk_Click);
            // 
            // btEditDateIssued
            // 
            this.btEditDateIssued.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEditDateIssued.Location = new System.Drawing.Point(319, 69);
            this.btEditDateIssued.Name = "btEditDateIssued";
            this.btEditDateIssued.Size = new System.Drawing.Size(39, 23);
            this.btEditDateIssued.TabIndex = 43;
            this.btEditDateIssued.Text = "Edit";
            this.btEditDateIssued.UseVisualStyleBackColor = true;
            this.btEditDateIssued.Click += new System.EventHandler(this.btEditDateIssued_Click);
            // 
            // btEditDateRaised
            // 
            this.btEditDateRaised.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEditDateRaised.Location = new System.Drawing.Point(319, 45);
            this.btEditDateRaised.Name = "btEditDateRaised";
            this.btEditDateRaised.Size = new System.Drawing.Size(39, 23);
            this.btEditDateRaised.TabIndex = 42;
            this.btEditDateRaised.Text = "Edit";
            this.btEditDateRaised.UseVisualStyleBackColor = true;
            this.btEditDateRaised.Click += new System.EventHandler(this.btEditDateRasied_Click);
            // 
            // cbClerk
            // 
            this.cbClerk.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbClerk.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbClerk.FormattingEnabled = true;
            this.cbClerk.Location = new System.Drawing.Point(126, 95);
            this.cbClerk.Name = "cbClerk";
            this.cbClerk.Size = new System.Drawing.Size(187, 24);
            this.cbClerk.TabIndex = 5;
            // 
            // cbPriority
            // 
            this.cbPriority.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPriority.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPriority.FormattingEnabled = true;
            this.cbPriority.Location = new System.Drawing.Point(126, 123);
            this.cbPriority.Name = "cbPriority";
            this.cbPriority.Size = new System.Drawing.Size(187, 24);
            this.cbPriority.TabIndex = 6;
            // 
            // dtDateIssued
            // 
            this.dtDateIssued.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtDateIssued.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateIssued.Location = new System.Drawing.Point(126, 69);
            this.dtDateIssued.Name = "dtDateIssued";
            this.dtDateIssued.Size = new System.Drawing.Size(187, 22);
            this.dtDateIssued.TabIndex = 4;
            // 
            // dtDateRaised
            // 
            this.dtDateRaised.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtDateRaised.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDateRaised.Location = new System.Drawing.Point(126, 45);
            this.dtDateRaised.Name = "dtDateRaised";
            this.dtDateRaised.Size = new System.Drawing.Size(187, 22);
            this.dtDateRaised.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(14, 126);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 16);
            this.label3.TabIndex = 40;
            this.label3.Text = "Priority";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(14, 98);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 16);
            this.label5.TabIndex = 38;
            this.label5.Text = "Clerk";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(14, 74);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 16);
            this.label6.TabIndex = 37;
            this.label6.Text = "Date Issued";
            // 
            // tbJobNumber
            // 
            this.tbJobNumber.Location = new System.Drawing.Point(126, 18);
            this.tbJobNumber.Name = "tbJobNumber";
            this.tbJobNumber.Size = new System.Drawing.Size(187, 22);
            this.tbJobNumber.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(14, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 16);
            this.label7.TabIndex = 36;
            this.label7.Text = "Date Raised";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(14, 24);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 16);
            this.label8.TabIndex = 35;
            this.label8.Text = "Work Order";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btEditCustomer);
            this.groupBox2.Controls.Add(this.tbTelNo);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.tbPostCode);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.tbAddress3);
            this.groupBox2.Controls.Add(this.tbAddress2);
            this.groupBox2.Controls.Add(this.tbAddress1);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.tbCustomerName);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(410, 41);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(383, 207);
            this.groupBox2.TabIndex = 36;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Customer Details";
            // 
            // btEditCustomer
            // 
            this.btEditCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEditCustomer.Location = new System.Drawing.Point(18, 176);
            this.btEditCustomer.Name = "btEditCustomer";
            this.btEditCustomer.Size = new System.Drawing.Size(55, 23);
            this.btEditCustomer.TabIndex = 52;
            this.btEditCustomer.Text = "Edit";
            this.btEditCustomer.UseVisualStyleBackColor = true;
            this.btEditCustomer.Click += new System.EventHandler(this.btEditCustomer_Click);
            // 
            // tbTelNo
            // 
            this.tbTelNo.Location = new System.Drawing.Point(135, 149);
            this.tbTelNo.Name = "tbTelNo";
            this.tbTelNo.Size = new System.Drawing.Size(240, 22);
            this.tbTelNo.TabIndex = 12;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(15, 152);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 16);
            this.label9.TabIndex = 50;
            this.label9.Text = "Phone";
            // 
            // tbPostCode
            // 
            this.tbPostCode.Location = new System.Drawing.Point(135, 123);
            this.tbPostCode.Name = "tbPostCode";
            this.tbPostCode.Size = new System.Drawing.Size(240, 22);
            this.tbPostCode.TabIndex = 11;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(15, 126);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 16);
            this.label10.TabIndex = 49;
            this.label10.Text = "Post Code";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(15, 100);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 16);
            this.label12.TabIndex = 47;
            this.label12.Text = "Address 3";
            // 
            // tbAddress3
            // 
            this.tbAddress3.Location = new System.Drawing.Point(135, 97);
            this.tbAddress3.Name = "tbAddress3";
            this.tbAddress3.Size = new System.Drawing.Size(240, 22);
            this.tbAddress3.TabIndex = 10;
            // 
            // tbAddress2
            // 
            this.tbAddress2.Location = new System.Drawing.Point(135, 71);
            this.tbAddress2.Name = "tbAddress2";
            this.tbAddress2.Size = new System.Drawing.Size(240, 22);
            this.tbAddress2.TabIndex = 9;
            // 
            // tbAddress1
            // 
            this.tbAddress1.Location = new System.Drawing.Point(135, 47);
            this.tbAddress1.Name = "tbAddress1";
            this.tbAddress1.Size = new System.Drawing.Size(240, 22);
            this.tbAddress1.TabIndex = 8;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(15, 74);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(69, 16);
            this.label14.TabIndex = 45;
            this.label14.Text = "Address 2";
            // 
            // tbCustomerName
            // 
            this.tbCustomerName.Location = new System.Drawing.Point(135, 21);
            this.tbCustomerName.Name = "tbCustomerName";
            this.tbCustomerName.Size = new System.Drawing.Size(240, 22);
            this.tbCustomerName.TabIndex = 7;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(15, 50);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(69, 16);
            this.label15.TabIndex = 44;
            this.label15.Text = "Address 1";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(15, 24);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(105, 16);
            this.label16.TabIndex = 43;
            this.label16.Text = "Customer Name";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.gridWorkDetail);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(12, 376);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(710, 109);
            this.groupBox3.TabIndex = 37;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Work Order Jobs";
            // 
            // gridWorkDetail
            // 
            this.gridWorkDetail.AllowUserToAddRows = false;
            this.gridWorkDetail.AllowUserToDeleteRows = false;
            this.gridWorkDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridWorkDetail.Location = new System.Drawing.Point(6, 21);
            this.gridWorkDetail.MultiSelect = false;
            this.gridWorkDetail.Name = "gridWorkDetail";
            this.gridWorkDetail.ReadOnly = true;
            this.gridWorkDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridWorkDetail.Size = new System.Drawing.Size(696, 76);
            this.gridWorkDetail.TabIndex = 19;
            this.gridWorkDetail.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridWorkDetail_KeyDown);
            // 
            // btSave
            // 
            this.btSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btSave.Location = new System.Drawing.Point(12, 254);
            this.btSave.Name = "btSave";
            this.btSave.Size = new System.Drawing.Size(89, 23);
            this.btSave.TabIndex = 13;
            this.btSave.Text = "Save W/O";
            this.btSave.UseVisualStyleBackColor = true;
            this.btSave.Click += new System.EventHandler(this.btSave_Click);
            // 
            // btNew
            // 
            this.btNew.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btNew.Location = new System.Drawing.Point(12, 598);
            this.btNew.Name = "btNew";
            this.btNew.Size = new System.Drawing.Size(89, 23);
            this.btNew.TabIndex = 19;
            this.btNew.Text = "New Job";
            this.btNew.UseVisualStyleBackColor = true;
            this.btNew.Click += new System.EventHandler(this.btNew_Click);
            // 
            // btDelete
            // 
            this.btDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btDelete.Location = new System.Drawing.Point(108, 598);
            this.btDelete.Name = "btDelete";
            this.btDelete.Size = new System.Drawing.Size(89, 23);
            this.btDelete.TabIndex = 20;
            this.btDelete.Text = "Delete Job";
            this.btDelete.UseVisualStyleBackColor = true;
            this.btDelete.Click += new System.EventHandler(this.btDelete_Click);
            // 
            // btAmend
            // 
            this.btAmend.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAmend.Location = new System.Drawing.Point(203, 598);
            this.btAmend.Name = "btAmend";
            this.btAmend.Size = new System.Drawing.Size(89, 23);
            this.btAmend.TabIndex = 21;
            this.btAmend.Text = "Change Job";
            this.btAmend.UseVisualStyleBackColor = true;
            this.btAmend.Click += new System.EventHandler(this.btAmend_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btEditComment);
            this.groupBox4.Controls.Add(this.btStatus);
            this.groupBox4.Controls.Add(this.tbComments);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.dtStatusDate);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.tbStatus);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(12, 283);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(971, 87);
            this.groupBox4.TabIndex = 38;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Status";
            // 
            // btEditComment
            // 
            this.btEditComment.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEditComment.Location = new System.Drawing.Point(917, 18);
            this.btEditComment.Name = "btEditComment";
            this.btEditComment.Size = new System.Drawing.Size(39, 23);
            this.btEditComment.TabIndex = 49;
            this.btEditComment.Text = "Edit";
            this.btEditComment.UseVisualStyleBackColor = true;
            this.btEditComment.Click += new System.EventHandler(this.btEditComment_Click);
            // 
            // btStatus
            // 
            this.btStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btStatus.Location = new System.Drawing.Point(319, 17);
            this.btStatus.Name = "btStatus";
            this.btStatus.Size = new System.Drawing.Size(39, 23);
            this.btStatus.TabIndex = 48;
            this.btStatus.Text = "Edit";
            this.btStatus.UseVisualStyleBackColor = true;
            this.btStatus.Click += new System.EventHandler(this.btStatus_Click);
            // 
            // tbComments
            // 
            this.tbComments.Location = new System.Drawing.Point(533, 18);
            this.tbComments.Margin = new System.Windows.Forms.Padding(0);
            this.tbComments.Multiline = true;
            this.tbComments.Name = "tbComments";
            this.tbComments.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbComments.Size = new System.Drawing.Size(372, 52);
            this.tbComments.TabIndex = 18;
            this.tbComments.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(413, 21);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(72, 16);
            this.label11.TabIndex = 47;
            this.label11.Text = "Comments";
            // 
            // dtStatusDate
            // 
            this.dtStatusDate.CustomFormat = "dd/MM/yyyy hh:mm";
            this.dtStatusDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtStatusDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtStatusDate.Location = new System.Drawing.Point(126, 48);
            this.dtStatusDate.Name = "dtStatusDate";
            this.dtStatusDate.Size = new System.Drawing.Size(187, 22);
            this.dtStatusDate.TabIndex = 17;
            this.dtStatusDate.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 16);
            this.label1.TabIndex = 46;
            this.label1.Text = "Status Date";
            // 
            // tbStatus
            // 
            this.tbStatus.Location = new System.Drawing.Point(126, 18);
            this.tbStatus.Name = "tbStatus";
            this.tbStatus.Size = new System.Drawing.Size(187, 22);
            this.tbStatus.TabIndex = 16;
            this.tbStatus.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(14, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 16);
            this.label2.TabIndex = 45;
            this.label2.Text = "Status";
            // 
            // btHistory
            // 
            this.btHistory.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btHistory.Location = new System.Drawing.Point(107, 254);
            this.btHistory.Name = "btHistory";
            this.btHistory.Size = new System.Drawing.Size(100, 23);
            this.btHistory.TabIndex = 14;
            this.btHistory.Text = "History";
            this.btHistory.UseVisualStyleBackColor = true;
            this.btHistory.Click += new System.EventHandler(this.btHistory_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.gridWorkTotals);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(12, 491);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(710, 101);
            this.groupBox5.TabIndex = 41;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Work Order Totals";
            // 
            // gridWorkTotals
            // 
            this.gridWorkTotals.AllowUserToAddRows = false;
            this.gridWorkTotals.AllowUserToDeleteRows = false;
            this.gridWorkTotals.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridWorkTotals.Location = new System.Drawing.Point(6, 21);
            this.gridWorkTotals.MultiSelect = false;
            this.gridWorkTotals.Name = "gridWorkTotals";
            this.gridWorkTotals.ReadOnly = true;
            this.gridWorkTotals.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gridWorkTotals.Size = new System.Drawing.Size(696, 67);
            this.gridWorkTotals.TabIndex = 19;
            this.gridWorkTotals.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gridWorkTotals_KeyDown);
            // 
            // btClose
            // 
            this.btClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btClose.Location = new System.Drawing.Point(298, 598);
            this.btClose.Name = "btClose";
            this.btClose.Size = new System.Drawing.Size(89, 23);
            this.btClose.TabIndex = 22;
            this.btClose.Text = "Close";
            this.btClose.UseVisualStyleBackColor = true;
            this.btClose.Click += new System.EventHandler(this.btClose_Click);
            // 
            // WorkOrderDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1142, 625);
            this.Controls.Add(this.btClose);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.btHistory);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.btAmend);
            this.Controls.Add(this.btDelete);
            this.Controls.Add(this.btNew);
            this.Controls.Add(this.btSave);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txWorkID);
            this.Controls.Add(this.btSearchWO);
            this.Name = "WorkOrderDetail";
            this.Text = "Work Order Detail";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.WorkOrderDetail_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResizeEnd += new System.EventHandler(this.Form1_ResizeEnd);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.WorkOrderDetail_KeyUp);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridWorkDetail)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridWorkTotals)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btSearchWO;
        private System.Windows.Forms.TextBox txWorkID;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView gridWorkDetail;
        private System.Windows.Forms.TextBox tbJobNumber;
        private System.Windows.Forms.TextBox tbTelNo;
        private System.Windows.Forms.TextBox tbPostCode;
        private System.Windows.Forms.TextBox tbAddress3;
        private System.Windows.Forms.TextBox tbAddress2;
        private System.Windows.Forms.TextBox tbAddress1;
        private System.Windows.Forms.TextBox tbCustomerName;
        private System.Windows.Forms.Button btSave;
        private System.Windows.Forms.DateTimePicker dtDateRaised;
        private System.Windows.Forms.DateTimePicker dtDateIssued;
        private System.Windows.Forms.Button btNew;
        private System.Windows.Forms.Button btDelete;
        private System.Windows.Forms.Button btAmend;
        private System.Windows.Forms.ComboBox cbPriority;
        private System.Windows.Forms.ComboBox cbClerk;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DateTimePicker dtStatusDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbStatus;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbComments;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btHistory;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DataGridView gridWorkTotals;
        private System.Windows.Forms.Button btClose;
        private System.Windows.Forms.Button btEditDateIssued;
        private System.Windows.Forms.Button btEditDateRaised;
        private System.Windows.Forms.Button btEditPriority;
        private System.Windows.Forms.Button btEditClerk;
        private System.Windows.Forms.Button btEditComment;
        private System.Windows.Forms.Button btStatus;
        private System.Windows.Forms.Button btEditDateOnSite;
        private System.Windows.Forms.Label lblDateOnSite;
        private System.Windows.Forms.Button btEditDateCompleted;
        private System.Windows.Forms.Label lblDateCompleted;
        private System.Windows.Forms.TextBox tbDateCompleted;
        private System.Windows.Forms.TextBox tbDateOnSite;
        private System.Windows.Forms.Button btEditJobNumber;
        private System.Windows.Forms.Button btEditCustomer;
    }
}

