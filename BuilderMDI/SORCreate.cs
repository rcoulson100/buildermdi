﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BuilderMDI
{
    public partial class SORCreate : Form
    {
        public enum ScreenMode
        {
            Create,
            Edit
        }

        SqlConnection myConnection;
        ScreenMode myMode;
        Form myParent;
        int mySORID = -1;
        String myCurrentDesc = "";
        String myRated = "Yes";

        public SORCreate(Form p, SqlConnection cnn, ScreenMode mode, int sorID = -1, String description = "", String rated = "Yes")
        {
            InitializeComponent();
            this.Icon = BuilderMDI.Properties.Resources.ICON;
            myConnection = cnn;
            myMode = mode;
            myParent = p;
            mySORID = sorID;
            myCurrentDesc = description;
            myRated = rated;
        }

        private void SORCreate_Load(object sender, EventArgs e)
        {
            Form frm = sender as Form;

            PopulateListbox();

            switch (myMode)
            {
                case ScreenMode.Create:
                    this.Text = "SOR Create";
                    tbDescription.BackColor = Color.Cornsilk;
                    break;

                case ScreenMode.Edit:
                    this.Text = "SOR Edit";
                    tbDescription.BackColor = Color.Cornsilk;
                    tbDescription.Text = myCurrentDesc;
                    cbRated.Text = myRated;
                    break;

                default:
                    break;
            }

            SetScreen(frm);
        }

        private void SetScreen(Form frm)
        {
            //if (frm.Width <= 600)
            //    frm.Width = 600;

            //groupBox1.Left = 5;
            //groupBox1.Top = 5;
            //groupBox1.Width = frm.ClientSize.Width - groupBox1.Left - 5;
            //groupBox1.Height = frm.ClientSize.Height - groupBox1.Top - btOK.Height - 10;

            //btOK.Left = 5;
            //btOK.Top = frm.Height - btOK.Height - 45;
        }

        private void SetButtonStates()
        {
            btOK.Enabled = true;
            return;
        }

        private void PopulateListbox()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Name", typeof(string));
            dt.Columns.Add("Value", typeof(int));
            dt.Rows.Add("Yes", 1);
            dt.Rows.Add("No", 0);

            cbRated.DataSource = dt;
            cbRated.DisplayMember = "Name";
        }

        private void SORCreate_Shown(object sender, EventArgs e)
        {
            SetButtonStates();
        }

        protected override void OnShown(EventArgs e)
        {
            tbDescription.Focus();
            base.OnShown(e);
        }

        private void SORCreate_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (myParent.GetType() == typeof(SOR))
            {
                SOR s = myParent as SOR;
                Globals.RestoreScreen(myParent);
                s.ForceRefresh();
            }
        }

        private void btOK_Click(object sender, EventArgs e)
        {
            if (tbDescription.Text.Trim().Length == 0)
            {
                MessageBox.Show("SOR Description must be entered.", "Work Order", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (tbDescription.Text.Trim().Length > 100)
            {
                MessageBox.Show("SOR Description is too long. Please use 100 characters or less.", "Work Order", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int rated = 1;

            // It was populated with data table hence the selected value is a data row
            DataRowView rRated = (DataRowView)cbRated.SelectedValue;

            // Retreive as string as this is the most 'generic' data type
            string tmpRated = rRated.Row["Value"].ToString();

            //Convert to integer if possible
            if (!int.TryParse(tmpRated, out rated))
            {
                MessageBox.Show("Failed to rated value from rated from list.", "Work Order", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (rated < 0 || rated > 1)
                rated = 1;

            if (myMode == ScreenMode.Create)
            {
                if (DoCreate(tbDescription.Text.Trim(), rated) == 0)
                {
                    this.Close();
                }
            }
            else
            {
                if (mySORID == -1)
                {
                    MessageBox.Show("System error. Screen is in edit mode but the SOR ID has not been set.", "Work Order", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (DoEdit(mySORID, tbDescription.Text.Trim(), rated) == 0)
                {
                    this.Close();
                }
            }
        }

        private int DoCreate(String sorDesc, int rated)
        {
            int retval = -1;

            try
            {
                // Set uyp the command object
                SqlCommand command = new SqlCommand("sp_SOR_Insert", myConnection);
                command.CommandType = CommandType.StoredProcedure;

                // Add all parmeters including the output parameters here
                command.Parameters.Add(new SqlParameter("@SORDesc", sorDesc));
                command.Parameters.Add(new SqlParameter("@Rated", rated));

                SqlParameter parRetval = command.Parameters.Add("@retval", SqlDbType.Int);
                parRetval.Direction = ParameterDirection.ReturnValue;

                // Execute the SP
                command.ExecuteNonQuery();

                retval = (int)command.Parameters["@retval"].Value;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
                retval = -1;
            }

            return retval;
        }

        private int DoEdit(int sorID, String sorDesc, int rated)
        {
            int retval = -1;

            try
            {
                // Set uyp the command object
                SqlCommand command = new SqlCommand("sp_SOR_Update", myConnection);
                command.CommandType = CommandType.StoredProcedure;

                // Add all parmeters including the output parameters here
                command.Parameters.Add(new SqlParameter("@SORID", sorID));
                command.Parameters.Add(new SqlParameter("@SORDesc", sorDesc));
                command.Parameters.Add(new SqlParameter("@Rated", rated));

                SqlParameter parRetval = command.Parameters.Add("@retval", SqlDbType.Int);
                parRetval.Direction = ParameterDirection.ReturnValue;

                // Execute the SP
                command.ExecuteNonQuery();

                retval = (int)command.Parameters["@retval"].Value;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
                retval = -1;
            }

            return retval;
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
