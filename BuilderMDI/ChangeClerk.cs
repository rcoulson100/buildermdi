﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BuilderMDI
{
    public partial class ChangeClerk : Form
    {
        SqlConnection myConnection;
        int WorkID;
        String theClerk;
        Form myParent;

        public ChangeClerk(Form parent, SqlConnection cnn, int workId, String clerk)
        {
            InitializeComponent();
            this.Icon = BuilderMDI.Properties.Resources.ICON;
            myConnection = cnn;
            WorkID = workId;
            theClerk = clerk;
            myParent = parent;
        }

        private void btSave_Click(object sender, EventArgs e)
        {

            int retval = -1;

            try
            {
                int clerkId = 1;

                // It was populated with data table hence the selected value is a data row
                DataRowView rClerk = (DataRowView)cbClerk.SelectedValue;

                // Retreive as string as this is the most 'generic' data type
                string tmpClerk = rClerk.Row["ClerkID"].ToString();

                //Convert to integer if possible
                if (!int.TryParse(tmpClerk, out clerkId))
                {
                    MessageBox.Show("Failed to convert clerk from list.", "Work Order", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                // Set up the command object
                SqlCommand command = new SqlCommand("sp_WorkHead_Update_Clerk", myConnection);
                command.CommandType = CommandType.StoredProcedure;

                // Add all parmeters including the output parameters here
                command.Parameters.Add(new SqlParameter("@WorkID", WorkID));
                command.Parameters.Add(new SqlParameter("@ClerkID", clerkId));

                SqlParameter parRetval = command.Parameters.Add("@retval", SqlDbType.Int);
                parRetval.Direction = ParameterDirection.ReturnValue;

                // Execute the SP
                command.ExecuteNonQuery();

                retval = (int)command.Parameters["@retval"].Value;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
                retval = -1;
            }

            if (retval == 0)
            {
                //if (myParent.GetType() == typeof(Main))
                //{
                //    var m = myParent as Main;
                //    m.RestoreScreen(true);
                //}

                //myParent.ForceGridRefresh();
                this.Close();
            }
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ChangeClerk_Load(object sender, EventArgs e)
        {

            // populate the clerks listbox
            SqlCommand cmd = new SqlCommand("sp_Listbox_Clerks_Select", myConnection);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataReader rClerk = cmd.ExecuteReader();
            DataTable clerkData = new DataTable();
            clerkData.Load(rClerk);

            cbClerk.DataSource = clerkData;
            cbClerk.DisplayMember = "Name";

            cbClerk.Enabled = true;
            cbClerk.BackColor = Color.Cornsilk;

            cbClerk.Text = theClerk;
        }

        private void ChangeClerk_FormClosed(object sender, FormClosedEventArgs e)
        {
            Globals.RestoreScreen(myParent);

            if (myParent.GetType() == typeof(Main))
            {
                var m = myParent as Main;
                m.RestoreScreen(true);
            }
            else if (myParent.GetType() == typeof(WorkOrderDetail))
            {
                Globals.RestoreScreen(myParent);

                var m = myParent as WorkOrderDetail;
                m.ForceGridRefresh(true);
            }
            else
            {
                Globals.RestoreScreen(myParent);
            }
        }
    }
}
