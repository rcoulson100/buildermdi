﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BuilderMDI
{
    public partial class UsersCreate : Form
    {
        SqlConnection myConnection;
        Form myParent;
  
        public UsersCreate(Form p, SqlConnection cnn)
        {
            InitializeComponent();
            this.Icon = BuilderMDI.Properties.Resources.ICON;
            myConnection = cnn;
            myParent = p;
        }


        private void UsersCreate_Load(object sender, EventArgs e)
        {
            Form frm = sender as Form;

            tbName.BackColor = Color.Cornsilk;
            tbNewPin.BackColor = Color.Cornsilk;
            tbConfirmPin.BackColor = Color.Cornsilk;
            SetScreen(frm);
        }

        private void SetScreen(Form frm)
        {
            //groupBox1.Left = 5;
            //groupBox1.Top = 5;
            //groupBox1.Width = frm.ClientSize.Width - groupBox1.Left - 5;
            //groupBox1.Height = frm.ClientSize.Height - groupBox1.Top - btOK.Height - 10;

            //btOK.Left = 5;
            //btOK.Top = frm.Height - btOK.Height - 45;
        }

        private void SetButtonStates()
        {
            btOK.Enabled = true;
            return;
        }

        private void UsersCreate_Shown(object sender, EventArgs e)
        {
            SetButtonStates();
        }
        
        protected override void OnShown(EventArgs e)
        {
            tbName.Focus();
            base.OnShown(e);
        }

        private void UsersCreate_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (myParent.GetType() == typeof(Users))
            {
                Users u = myParent as Users;
                Globals.RestoreScreen(myParent);
                u.ForceRefresh();
            }
        }

        private void btOK_Click(object sender, EventArgs e)
        {
            if (tbName.Text.Trim().Length == 0)
            {
                MessageBox.Show("User Name must be entered.", "New User", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (tbName.Text.Trim().Length > 50)
            {
                MessageBox.Show("User Name is too long. Please use 50 characters or less.", "New User", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            
            // Confirm New pins match
            if (tbConfirmPin.Text != tbNewPin.Text)
            {
                MessageBox.Show("Pin Confirmation does not match PIN.", "New User", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (tbNewPin.Text.Trim().Length < 4 || tbNewPin.Text.Trim().Length > 6)
            {
                MessageBox.Show("Please use a PIN that is between 4 and 6 numbers.", "New User", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            int newPin = -1;
            if (!int.TryParse(tbNewPin.Text, out newPin))
            {
                MessageBox.Show("New PIN must be numeric", "New User", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (DoCreate(tbName.Text, newPin) == 0)
            {
                this.Close();
            }
        }

        private int DoCreate(String name, int pin)
        {
            int retval = -1;

            try
            {
                // Set uyp the command object
                SqlCommand command = new SqlCommand("sp_Users_Insert", myConnection);
                command.CommandType = CommandType.StoredProcedure;

                // Add all parmeters including the output parameters here
                command.Parameters.Add(new SqlParameter("@Name", name));
                command.Parameters.Add(new SqlParameter("@Pin", pin));

                SqlParameter parRetval = command.Parameters.Add("@retval", SqlDbType.Int);
                parRetval.Direction = ParameterDirection.ReturnValue;

                // Execute the SP
                command.ExecuteNonQuery();

                retval = (int)command.Parameters["@retval"].Value;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
                retval = -1;
            }

            return retval;
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
