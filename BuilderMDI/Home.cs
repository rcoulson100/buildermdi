﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;

namespace BuilderMDI
{
    public partial class Home : Form
    {
        SqlConnection myConnection;
        
        public Home()
        {
            InitializeComponent();
            this.Icon = BuilderMDI.Properties.Resources.ICON;
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void Home_Load(object sender, EventArgs e)
        {
            bool bUseSecurity = true;

            try
            {

                SqlConnectionStringBuilder strBuilder = new SqlConnectionStringBuilder();
                strBuilder.ConnectTimeout = 30;
                //strBuilder.Encrypt = true;

                strBuilder.UserID = ConfigurationManager.AppSettings["DBUSER"].ToString();
                strBuilder.Password = ConfigurationManager.AppSettings["DBKEY"].ToString();
                strBuilder.InitialCatalog = ConfigurationManager.AppSettings["DBNAME"].ToString();
                strBuilder.DataSource = ConfigurationManager.AppSettings["DBSRV"].ToString();

                myConnection = new SqlConnection(strBuilder.ConnectionString);

                myConnection.Open();
            }
            catch (Exception er)
            {
                MessageBox.Show(er.ToString());
                Application.Exit();
            }

            if (bUseSecurity)
            {
                // Check login
                Login login = new Login(myConnection);
                login.ShowDialog(this);
            }

            this.Size = new Size(1500, 800);
        }

        private void Home_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (myConnection.State == ConnectionState.Open)
            {
                myConnection.Close();
            }
            myConnection.Dispose();
        }

        private void searchWorksOrderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WorkOrderDetail wordOrderDetail = new WorkOrderDetail(this, myConnection, ScreenMode.Search, "");
            wordOrderDetail.MdiParent = this;
            //wordOrderDetail.WindowState = FormWindowState.Maximized;
            wordOrderDetail.Show();
        }

        private bool SetChildFocus(Type t)
        {
            foreach (Form frm in this.MdiChildren)
            {
                if (frm.GetType() == t)
                {
                    if (frm.Enabled)
                        frm.Focus();

                    return true;
                }
            }
            return false;
        }

        private void viewWorksOrdersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!SetChildFocus(typeof(Main)))
            {
                Main main = new Main(this, myConnection);
                main.MdiParent = this;
                //main.WindowState = FormWindowState.Maximized;
                main.Show();
            }
        }

        private void weeklyJobStatusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Report1 reportFilter = new Report1(this, myConnection);
            reportFilter.ShowDialog(this);
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!SetChildFocus(typeof(WorkOrderDetail)))
            {
                WorkOrderDetail wordOrderDetail = new WorkOrderDetail(this, myConnection, ScreenMode.New, "");
                wordOrderDetail.MdiParent = this;
                //wordOrderDetail.WindowState = FormWindowState.Maximized;
                wordOrderDetail.Show();
            }
        }

        private void changePINToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!SetChildFocus(typeof(ChangePin)))
            {
                ChangePin changePin = new ChangePin(myConnection);
                changePin.MdiParent = Globals.MDIParentForm;
                changePin.Show();
                changePin.WindowState = FormWindowState.Normal;
            }
        }

        private void usersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!SetChildFocus(typeof(Users)))
            {
                Users users = new Users(this, myConnection);
                users.MdiParent = this;
                users.Show();
            }
        }

        private void cutomersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!SetChildFocus(typeof(Customers)))
            {
                Customers customers = new Customers(this, myConnection);
                customers.MdiParent = this;
                customers.Show();
            }
        }

        private void scheduleOfRatesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!SetChildFocus(typeof(SOR)))
            {
                SOR sor = new SOR(this, myConnection);
                sor.MdiParent = this;
                sor.Show();
            }
        }

        private void completedJobsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RepCompletedJobs reportFilter = new RepCompletedJobs(this, myConnection);
            reportFilter.ShowDialog(this);
        }
    }
}
