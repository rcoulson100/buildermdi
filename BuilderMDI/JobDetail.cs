﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BuilderMDI
{
    public enum JobDetailScreenMode
    {
        Create,
        Amend
    }

    public partial class JobDetail : Form
    {
        SqlConnection myConnection;
        int WorkID;
        int WorkLineID;
        JobDetailScreenMode screenMode = JobDetailScreenMode.Create;
        WorkOrderDetail myParent;
        bool bRatedSOR;

        public JobDetail(WorkOrderDetail parent, SqlConnection cnn, JobDetailScreenMode mode, int workId, int workLineId)
        {
            InitializeComponent();
            this.Icon = BuilderMDI.Properties.Resources.ICON;
            myConnection = cnn;
            WorkID = workId;
            WorkLineID = workLineId;
            screenMode = mode;
            myParent = parent;
        }

        private void WorkOrderJob_Load(object sender, EventArgs e)
        {
            tbLocation.BackColor = Color.Cornsilk;
            tbSurveyedUnits.BackColor = Color.Cornsilk;
            cbSOR.BackColor = Color.Cornsilk;

            PopulateListboxes();

            switch (screenMode)
            {
                case JobDetailScreenMode.Create:
                    break;

                case JobDetailScreenMode.Amend:
                    // get line details
                    PopulateScreen();
                    break;

                default:
                    MessageBox.Show("Unknown screen mode. Refer to you system supplier.");
                    break;
            }
            SetupScreen();
        }

        protected override void OnShown(EventArgs e)
        {
            tbLocation.Focus();
            base.OnShown(e);
        }

        private int PopulateScreen()
        {
            try
            {
                DataTable dt = new DataTable();
                SqlCommand command = new SqlCommand("sp_WorkDetail_Select", myConnection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add(new SqlParameter("@WorkId", WorkID));
                command.Parameters.Add(new SqlParameter("@WorkLineId", WorkLineID));

                SqlDataReader reader = command.ExecuteReader();
                dt.Load(reader);

                if (dt.Rows.Count == 0)
                {
                    MessageBox.Show("Failed to find Job Details.");
                }
                else if (dt.Rows.Count > 1)
                {
                    MessageBox.Show("Multiple Job Details found. Showing Line 1");
                }

                tbLocation.Text = dt.Rows[0]["Location"].ToString();
                cbSOR.Text = dt.Rows[0]["SOR"].ToString();
                tbSurveyedUnits.Text = dt.Rows[0]["SurveyedUnits"].ToString();
                tbActualUnits.Text = dt.Rows[0]["ActualUnits"].ToString();
                tbWorkDescription.Text = dt.Rows[0]["WorkDescription"].ToString();
                tbWorkRate.Text = dt.Rows[0]["WorkRate"].ToString();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
                return -1;
            }
            return 0;
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            int surveyedUnits = 0;
            int actualUnits = 0;
            double workRate = 0;

            //// Get the locaiton from the list box
            //int location = 1;

            //// It was populated with data table hence the selected value is a data row
            //DataRowView data = (DataRowView)cbLocation.SelectedValue;

            //// Retreive as string as this is the most 'generic' data type
            //string tmpLocation = data.Row["LocationID"].ToString();

            ////Convert to integer if possible
            //if (!int.TryParse(tmpLocation, out location))
            //{
            //    MessageBox.Show("Failed to convert location");
            //    return;
            //}

            // validate the location
            if (tbLocation.Text.Length == 0)
            {
                MessageBox.Show("Location must be specified.", "Job Details", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            // Get the SOR from the list box
            int sor = 1;

            // It was populated with data table hence the selected value is a data row
            DataRowView data = (DataRowView)cbSOR.SelectedValue;

            // Retreive as string as this is the most 'generic' data type
            string tmpSOR = data.Row["SORID"].ToString();

            //Convert to integer if possible
            if (!int.TryParse(tmpSOR, out sor))
            {
                MessageBox.Show("Failed to convert SOR.", "Job Details", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //if (!int.TryParse(tbLocation.Text, out location))
            //{
            //    MessageBox.Show("The Location field must be numeric.");
            //    return;
            //}

            if (!int.TryParse(tbSurveyedUnits.Text, out surveyedUnits))
            {
                MessageBox.Show("The Surveyed Units field must be numeric (no decimal places).", "Job Details", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (tbActualUnits.Text.Trim().Length > 0)
            {
                if (!int.TryParse(tbActualUnits.Text, out actualUnits))
                {
                    MessageBox.Show("The Actual Units field must be numeric (no decimal places).", "Job Details", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }

            if (tbWorkRate.Text.Trim().Length > 0)
            {
                if (!double.TryParse(tbWorkRate.Text, out workRate))
                {
                    MessageBox.Show("The Work Rate field must be numeric.", "Job Details", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (workRate > 99999)
                {
                    MessageBox.Show("The Work Rate is too big. Please split in to smaller Jobs.", "Job Details", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }

            switch (screenMode)
            {

                case JobDetailScreenMode.Create:
                    if (CreateWorkOrderDetail(WorkID, tbLocation.Text, surveyedUnits, actualUnits, sor, tbWorkDescription.Text, workRate))
                    {
                        //myParent.ForceGridRefresh();
                        this.Close();
                    }

                //if (newWorkLineId != -1)
                    //{
                    //    // Create the sor detail
                    //    if (CreateWorkOrderSOR(WorkID, newWorkLineId, sor) == 0)
                    //    {
                    //        myParent.ForceGridRefresh();
                    //        this.Close();
                    //    }
                    //}
                    break;

                case JobDetailScreenMode.Amend:
                    if (AmendWorkOrderDetail(WorkID, WorkLineID, tbLocation.Text, surveyedUnits, actualUnits, sor, tbWorkDescription.Text, workRate))
                    {
                        //myParent.ForceGridRefresh();
                        this.Close();
                    }
                    //if (retval != -1)
                    //{
                    //    // Amend the sor detail
                    //    if (AmendWorkOrderSOR(WorkID, WorkLineID, sor) == 0)
                    //    {
                    //        myParent.ForceGridRefresh();
                    //        this.Close();
                    //    }
                    //}
                    break;
            }

        }

        private void SetupScreen()
        {
            if (bRatedSOR)
            {
                tbWorkDescription.ReadOnly = true;
                tbWorkRate.ReadOnly = true;
                tbWorkDescription.BackColor = SystemColors.Control;
                tbWorkRate.BackColor = SystemColors.Control;
            }
            else
            {
                tbWorkDescription.ReadOnly = false;
                tbWorkRate.ReadOnly = false;
                tbWorkDescription.BackColor = Color.Cornsilk;
                tbWorkRate.BackColor = Color.Cornsilk;
            }
            return;
        }

        private void PopulateListboxes()
        {
            try
            {
                //// populate the locaiton list
                //SqlCommand locCommand = new SqlCommand("sp_Listbox_Locations_Select", myConnection);
                //locCommand.CommandType = CommandType.StoredProcedure;

                //SqlDataReader reader = locCommand.ExecuteReader();
                //DataTable locListData = new DataTable();
                //locListData.Load(reader);

                //cbLocation.DataSource = locListData;
                //cbLocation.DisplayMember = "LocationDesc";

                // populate the locaiton list
                SqlCommand sorCommand = new SqlCommand("sp_Listbox_SOR_Select", myConnection);
                sorCommand.CommandType = CommandType.StoredProcedure;

                SqlDataReader reader = sorCommand.ExecuteReader();
                DataTable sorListData = new DataTable();
                sorListData.Load(reader);

                cbSOR.DataSource = sorListData;
                cbSOR.DisplayMember = "SORDesc";
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }

        private bool CreateWorkOrderDetail(
            int WorkId,
            String Location,
            int SurveyedUnits,
            int ActualUnits,
            int SorID,
            String WorkDescription,
            double WorkRate)
        {
            //int iWorkLineID = -1;
            bool bRetval = false;

            try
            {
                // Set up the command object
                SqlCommand command = new SqlCommand("sp_WorkDetail_Insert", myConnection);
                command.CommandType = CommandType.StoredProcedure;

                // Add all parmeters including the output parameters here
                command.Parameters.Add(new SqlParameter("@WorkID", WorkId));
                command.Parameters.Add(new SqlParameter("@Location", Location));
                command.Parameters.Add(new SqlParameter("@SurveyedUnits", SurveyedUnits));
                command.Parameters.Add(new SqlParameter("@ActualUnits", ActualUnits));
                command.Parameters.Add(new SqlParameter("@SORID", SorID));
                command.Parameters.Add(new SqlParameter("@WorkDescription", WorkDescription));
                command.Parameters.Add(new SqlParameter("@WorkRate", WorkRate));

                //SqlParameter parWorkLineID = command.Parameters.Add("@WorkLineID", SqlDbType.Int);
                //parWorkLineID.Direction = ParameterDirection.Output;

                SqlParameter parRetval = command.Parameters.Add("@retval", SqlDbType.Int);
                parRetval.Direction = ParameterDirection.ReturnValue;

                // Execute the SP
                command.ExecuteNonQuery();

                int retval = (int)command.Parameters["@retval"].Value;
                if (retval != 0)
                {
                    MessageBox.Show("Failed to create new job", "New Job", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    bRetval = false;
                }
                else
                {
                    bRetval = true;
                }

                ////  0 is success
                //if (retval == 0)
                //    iWorkLineID = (int)command.Parameters["@WorkLineID"].Value;
                //else
                //    iWorkLineID = -1;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
                bRetval = false;
                //iWorkLineID = -1;
            }

            return bRetval;
        }

        private bool AmendWorkOrderDetail(
            int WorkId,
            int WorkLineId,
            String Location,
            int SurveyedUnits,
            int ActualUnits, 
            int SorID,
            String WorkDescription,
            double WorkRate)
        {
            bool bRetval = false;

            try
            {
                // Set up the command object
                SqlCommand command = new SqlCommand("sp_WorkDetail_Update", myConnection);
                command.CommandType = CommandType.StoredProcedure;

                // Add all parmeters including the output parameters here
                command.Parameters.Add(new SqlParameter("@WorkID", WorkId));
                command.Parameters.Add(new SqlParameter("@WorkLineID", WorkLineId));
                command.Parameters.Add(new SqlParameter("@Location", Location));
                command.Parameters.Add(new SqlParameter("@SurveyedUnits", SurveyedUnits));
                command.Parameters.Add(new SqlParameter("@ActualUnits", ActualUnits));
                command.Parameters.Add(new SqlParameter("@SORID", SorID));
                command.Parameters.Add(new SqlParameter("@WorkDescription", WorkDescription));
                command.Parameters.Add(new SqlParameter("@WorkRate", WorkRate));

                SqlParameter parRetval = command.Parameters.Add("@retval", SqlDbType.Int);
                parRetval.Direction = ParameterDirection.ReturnValue;

                // Execute the SP
                command.ExecuteNonQuery();

                int retval = (int)command.Parameters["@retval"].Value;
                if (retval != 0)
                {
                    MessageBox.Show("Failed to change job", "Change Job", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    bRetval = false;
                }
                else
                {
                    bRetval = true;
                }

            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
                bRetval = false;
            }

            return bRetval;
        }

        private int CreateWorkOrderSOR(
            int WorkId,
            int WorkLineId,
            int SorId)
        {
            int retval = -1;

            try
            {
                // Set up the command object
                SqlCommand command = new SqlCommand("sp_WorkSOR_Insert", myConnection);
                command.CommandType = CommandType.StoredProcedure;

                // Add all parmeters including the output parameters here
                command.Parameters.Add(new SqlParameter("@WorkID", WorkId));
                command.Parameters.Add(new SqlParameter("@WorkLineID", WorkLineId));
                command.Parameters.Add(new SqlParameter("@SORID", SorId));

                SqlParameter parRetval = command.Parameters.Add("@retval", SqlDbType.Int);
                parRetval.Direction = ParameterDirection.ReturnValue;

                // Execute the SP
                command.ExecuteNonQuery();

                retval = (int)command.Parameters["@retval"].Value;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
                retval = -1;
            }

            return retval;
        }

        private int AmendWorkOrderSOR(
            int WorkId,
            int WorkLineId,
            int SorId)
        {
            int retval = -1;

            try
            {
                // Set up the command object
                SqlCommand command = new SqlCommand("sp_WorkSOR_Update", myConnection);
                command.CommandType = CommandType.StoredProcedure;

                // Add all parmeters including the output parameters here
                command.Parameters.Add(new SqlParameter("@WorkID", WorkId));
                command.Parameters.Add(new SqlParameter("@WorkLineID", WorkLineId));
                command.Parameters.Add(new SqlParameter("@SORID", SorId));

                SqlParameter parRetval = command.Parameters.Add("@retval", SqlDbType.Int);
                parRetval.Direction = ParameterDirection.ReturnValue;

                // Execute the SP
                command.ExecuteNonQuery();

                retval = (int)command.Parameters["@retval"].Value;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
                retval = -1;
            }

            return retval;
        }

        private void cbSOR_SelectedValueChanged(object sender, EventArgs e)
        {
            // get the selected rated value from the listbox
            try
            {
                // It was populated with data table hence the selected value is a data row
                DataRowView data = (DataRowView)cbSOR.SelectedValue;
                bRatedSOR = (bool)data.Row["rated"];
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
                // just say it is rated
                bRatedSOR = true;
            }

            SetupScreen();
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void JobDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (myParent.GetType() == typeof(WorkOrderDetail))
            {
                Globals.RestoreScreen(myParent);

                var m = myParent as WorkOrderDetail;
                m.ForceGridRefresh(false);
            }
        }

    }
}
