﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BuilderMDI
{
    public partial class ChangeDate : Form
    {
        SqlConnection myConnection;
        int WorkID;
        DateTime theDate;
        Form myParent;
        String theDateType;

        public ChangeDate(Form parent, SqlConnection cnn, int workId, DateTime date, String dateType)
        {
            InitializeComponent();
            this.Icon = BuilderMDI.Properties.Resources.ICON;
            myConnection = cnn;
            WorkID = workId;
            theDate = date;
            myParent = parent;
            theDateType = dateType;
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            int retval = -1;

            try
            {
                DateTime newDate = dtDate.Value;

                // Set up the command object
                SqlCommand command = new SqlCommand("sp_WorkHead_Update_DateField", myConnection);
                command.CommandType = CommandType.StoredProcedure;

                // Add all parmeters including the output parameters here
                command.Parameters.Add(new SqlParameter("@WorkID", WorkID));
                command.Parameters.Add(new SqlParameter("@dateType", theDateType));
                command.Parameters.Add(new SqlParameter("@date", newDate));

                SqlParameter parRetval = command.Parameters.Add("@retval", SqlDbType.Int);
                parRetval.Direction = ParameterDirection.ReturnValue;

                // Execute the SP
                command.ExecuteNonQuery();

                retval = (int)command.Parameters["@retval"].Value;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
                retval = -1;
            }

            if (retval == 0)
            {
                //if (myParent.GetType() == typeof(Main))
                //{
                //    var m = myParent as Main;
                //    m.RestoreScreen(true);
                //}

                //myParent.ForceGridRefresh();
                this.Close();
            }
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ChangeDate_Load(object sender, EventArgs e)
        {
            switch (theDateType)
            {
                case "DateOnSite":
                    lbDate.Text = "Date On Site";
                    this.Text = "Change Date On Site";
                    dtDate.Format = DateTimePickerFormat.Custom;
                    dtDate.CustomFormat = "dd/MM/yyyy HH:mm"; 
                    break;

                case "DateCompleted":
                    lbDate.Text = "Date Completed";
                    this.Text = "Change Date Completed";
                    dtDate.Format = DateTimePickerFormat.Custom;
                    dtDate.CustomFormat = "dd/MM/yyyy HH:mm"; 
                    break;

                case "DateRaised":
                    lbDate.Text = "Date Raised";
                    this.Text = "Change Date Raised";
                    dtDate.Format = DateTimePickerFormat.Short;
                    break;

                case "DateIssued":
                    lbDate.Text = "Date Issued";
                    this.Text = "Change Date Issued";
                    dtDate.Format = DateTimePickerFormat.Short;
                    break;

                default:
                    this.Close();
                    break;
            }
            dtDate.Value = theDate;

        }

        private void ChangeDate_FormClosed(object sender, FormClosedEventArgs e)
        {
            Globals.RestoreScreen(myParent);

            if (myParent.GetType() == typeof(Main))
            {
                var m = myParent as Main;
                m.RestoreScreen(true);
            }
            else if (myParent.GetType() == typeof(WorkOrderDetail))
            {
                Globals.RestoreScreen(myParent);

                var m = myParent as WorkOrderDetail;
                m.ForceGridRefresh(true);
            }
            else
            {
                Globals.RestoreScreen(myParent);
            }
        }
    }
}
