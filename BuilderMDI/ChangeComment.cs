﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BuilderMDI
{
    public partial class ChangeComment : Form
    {
        SqlConnection myConnection;
        int WorkID;
        Form myParent;
        String Comments;

        public ChangeComment(Form parent, SqlConnection cnn, int workId, String comments)
        {
            InitializeComponent();
            this.Icon = BuilderMDI.Properties.Resources.ICON;
            myConnection = cnn;
            WorkID = workId;
            myParent = parent;
            Comments = comments;
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            int retval = -1;

            try
            {
                String comments = tbComment.Text;

                // Set up the command object
                SqlCommand command = new SqlCommand("sp_WorkHead_Update_Comment", myConnection);
                command.CommandType = CommandType.StoredProcedure;

                // Add all parmeters including the output parameters here
                command.Parameters.Add(new SqlParameter("@WorkID", WorkID));
                command.Parameters.Add(new SqlParameter("@Comments", comments));

                SqlParameter parRetval = command.Parameters.Add("@retval", SqlDbType.Int);
                parRetval.Direction = ParameterDirection.ReturnValue;

                // Execute the SP
                command.ExecuteNonQuery();

                retval = (int)command.Parameters["@retval"].Value;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
                retval = -1;
            }

            if (retval == 0)
            {
                //if (myParent.GetType() == typeof(Main))
                //{
                //    var m = myParent as Main;
                //    m.RestoreScreen(true);
                //}

                //myParent.ForceGridRefresh();
                this.Close();
            }
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ChangeComment_Load(object sender, EventArgs e)
        {
            tbComment.Text = Comments;
        }

        private void ChangeComment_FormClosed(object sender, FormClosedEventArgs e)
        {
            Globals.RestoreScreen(myParent);

            if (myParent.GetType() == typeof(Main))
            {
                var m = myParent as Main;
                m.RestoreScreen(true);
            }
            else if (myParent.GetType() == typeof(WorkOrderDetail))
            {
                Globals.RestoreScreen(myParent);

                var m = myParent as WorkOrderDetail;
                m.ForceGridRefresh(true);
            }
            else
            {
                Globals.RestoreScreen(myParent);
            }
        }
    }
}
