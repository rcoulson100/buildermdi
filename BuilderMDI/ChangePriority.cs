﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BuilderMDI
{
    public partial class ChangePriority : Form
    {
        SqlConnection myConnection;
        int WorkID;
        String thePriority;
        Form myParent;

        public ChangePriority(Form parent, SqlConnection cnn, int workId, String priority)
        {
            InitializeComponent();
            this.Icon = BuilderMDI.Properties.Resources.ICON;
            myConnection = cnn;
            WorkID = workId;
            thePriority = priority;
            myParent = parent;
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            int retval = -1;

            try
            {
                // Get the priority from the list box
                int priority = 1;

                // It was populated with data table hence the selected value is a data row
                DataRowView rPriority = (DataRowView)cbPriority.SelectedValue;

                // Retreive as string as this is the most 'generic' data type
                string tmpPriorty = rPriority.Row["Priority"].ToString();

                //Convert to integer if possible
                if (!int.TryParse(tmpPriorty, out priority))
                {
                    MessageBox.Show("Failed to convert priority from list.", "Work Order", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                // Set up the command object
                SqlCommand command = new SqlCommand("sp_WorkHead_Update_Priority", myConnection);
                command.CommandType = CommandType.StoredProcedure;

                // Add all parmeters including the output parameters here
                command.Parameters.Add(new SqlParameter("@WorkID", WorkID));
                command.Parameters.Add(new SqlParameter("@Priority", priority));

                SqlParameter parRetval = command.Parameters.Add("@retval", SqlDbType.Int);
                parRetval.Direction = ParameterDirection.ReturnValue;

                // Execute the SP
                command.ExecuteNonQuery();

                retval = (int)command.Parameters["@retval"].Value;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
                retval = -1;
            }

            if (retval == 0)
            {
                //if (myParent.GetType() == typeof(Main))
                //{
                //    var m = myParent as Main;
                //    m.RestoreScreen(true);
                //}

                //myParent.ForceGridRefresh();
                this.Close();
            }
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ChangePriority_Load(object sender, EventArgs e)
        {
            // populate the priority listbox
            SqlCommand cmd = new SqlCommand("sp_Listbox_Priority_Select", myConnection);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataReader rPriority = cmd.ExecuteReader();
            DataTable priorityData = new DataTable();
            priorityData.Load(rPriority);

            cbPriority.DataSource = priorityData;
            cbPriority.DisplayMember = "PriorityDesc";

            cbPriority.Text = thePriority;
        }

        private void ChangePriority_FormClosed(object sender, FormClosedEventArgs e)
        {
            Globals.RestoreScreen(myParent);

            if (myParent.GetType() == typeof(Main))
            {
                var m = myParent as Main;
                m.RestoreScreen(true);
            }
            else if (myParent.GetType() == typeof(WorkOrderDetail))
            {
                Globals.RestoreScreen(myParent);

                var m = myParent as WorkOrderDetail;
                m.ForceGridRefresh(true);
            }
            else
            {
                Globals.RestoreScreen(myParent);
            }
        }
    }
}
