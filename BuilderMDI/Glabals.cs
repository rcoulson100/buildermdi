﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.Collections;

namespace BuilderMDI
{
    public static class Globals
    {
        public static Form MDIParentForm;
        public static String UserName;
        public static Dictionary<String, Size> OldFormSizes = new Dictionary<String, Size>();

        public static void RestoreScreen(Form frm)
        {
            frm.Enabled = true;
            frm.WindowState = FormWindowState.Normal;
            if (OldFormSizes.ContainsKey(frm.Name))
                frm.Size = OldFormSizes[frm.Name];

        }

        public static void HideScreen(Form frm)
        {
            if (OldFormSizes.ContainsKey(frm.Name))
                OldFormSizes.Remove(frm.Name);
            OldFormSizes.Add(frm.Name, frm.Size);
            frm.Enabled = false;
            frm.WindowState = FormWindowState.Minimized;
        }

        public static void UpdateGridFont(DataGridView grid)
        {
            //Change cell font
            foreach (DataGridViewColumn c in grid.Columns)
            {
                c.DefaultCellStyle.Font = new Font("Arial", 12, GraphicsUnit.Pixel);
                c.HeaderCell.Style.Font = new Font("Arial", 12, GraphicsUnit.Pixel);
            }
        }

    }

    static partial class RS
    {
        public static class SPWORKHEADSELECT
        {
            public const string F1 = "JobNumber";
            public const string F2 = "DateRaised";
            public const string F3 = "DateIssued";
            public const string F4 = "RaisedBy";
            public const string F5 = "ClerkName";
            public const string F6 = "PriorityDesc";
            public const string F7 = "StatusDesc";
            public const string F8 = "StatusDate";
            public const string F9 = "Comments";
            public const string F10 = "CustomerName";
            public const string F11 = "Address1";
            public const string F12 = "Address2";
            public const string F13 = "Address3";
            public const string F14 = "Address4";
            public const string F15 = "Address5";
            public const string F16 = "PostCode";
            public const string F17 = "TelNo";
            public const string F18 = "StatusID";
        }
    }

    static partial class PA
    {
        public static class SPWORKHEADSELECT
        {
            public const string P1 = "@JobNumber";
        }
    }

}
