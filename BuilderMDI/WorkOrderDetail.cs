﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BuilderMDI
{

    public enum ScreenMode
    {
        New,
        Display,
        Search
    }

    public enum EditField
    {
        None,
        WorkOrder,
        DateRaised,
        DateIssued,
        Clerk,
        Priority,
        DateOnSite,
        DateCompleted,
        Customer,
        Status,
        Comments
    }

    public partial class WorkOrderDetail : Form
    {
        SqlConnection myConnection;
        ScreenMode screenMode = ScreenMode.Display;
        String jobNumber = "";
        int gWorkID;
        int gCustomerID;
        Form myParent;
        int gStatus = -1;
        DateTime gDateOnSite;
        Boolean gHasDateOnSite = false;
        DateTime gDateCompleted;
        Boolean gHasDateCompleted = false;
        EditField editing;

        public WorkOrderDetail(Form p, SqlConnection cnn, ScreenMode mode, String job)
        {
            InitializeComponent();
            this.Icon = BuilderMDI.Properties.Resources.ICON;
            myConnection = cnn;
            screenMode = mode;
            jobNumber = job;
            myParent = p;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Form frm = sender as Form;
            frm.Size = new Size(1300, 700);
            SetScreen(frm);

            PopulateListboxes();

            switch (screenMode)
            {
                case ScreenMode.Display:
                    txWorkID.Text = jobNumber;
                    SetScreenAsDisplay();
                    GetData(jobNumber);
                    break;

                case ScreenMode.Search:
                    txWorkID.Enabled = true;
                    txWorkID.BackColor = Color.Cornsilk;
                    btSearchWO.Enabled = true;
                    btSave.Enabled = false;

                    tbJobNumber.ReadOnly = true;
                    //tbDateIssued.ReadOnly = true;
                    //tbDateRaised.ReadOnly = true;
                    dtDateIssued.Enabled = false;
                    dtDateRaised.Enabled = false;
                    tbComments.ReadOnly = true;

                    //tbRaisedBy.ReadOnly = true;
                    //tbClerk.ReadOnly = true;
                    cbClerk.Enabled = false;
                    //tbPriority.ReadOnly = true;
                    cbPriority.Enabled = false;
                    tbStatus.ReadOnly = true;
                    dtStatusDate.Enabled = false;

                    tbCustomerName.ReadOnly = true;
                    tbAddress1.ReadOnly = true;
                    tbAddress2.ReadOnly = true;
                    tbAddress3.ReadOnly = true;
                    //tbAddress4.ReadOnly = true;
                    //tbAddress5.ReadOnly = true;
                    tbPostCode.ReadOnly = true;
                    tbTelNo.ReadOnly = true;

                    break;

                case ScreenMode.New:
                    txWorkID.Text = jobNumber;
                    txWorkID.Enabled = false;
                    btSearchWO.Enabled = false;
                    btSave.Enabled = true;

                    tbJobNumber.ReadOnly = false;
                    tbJobNumber.BackColor = Color.Cornsilk;
                    //tbDateIssued.ReadOnly = false;
                    //tbDateRaised.ReadOnly = false;
                    dtDateIssued.Enabled = true;
                    dtDateRaised.Enabled = true;
                    //tbRaisedBy.ReadOnly = false;
                    //tbClerk.ReadOnly = false;
                    cbClerk.Enabled = true;
                    cbClerk.BackColor = Color.Cornsilk;
                    //tbPriority.ReadOnly = false;
                    cbPriority.Enabled = true;
                    cbPriority.BackColor = Color.Cornsilk;
                    tbStatus.ReadOnly = true;
                    dtStatusDate.Enabled = false;
                    tbComments.ReadOnly = true;

                    tbCustomerName.ReadOnly = false;
                    tbCustomerName.BackColor = Color.Cornsilk;
                    tbAddress1.ReadOnly = false;
                    tbAddress1.BackColor = Color.Cornsilk;
                    tbAddress2.ReadOnly = false;
                    tbAddress3.ReadOnly = false;
                    //tbAddress4.ReadOnly = false;
                    //tbAddress5.ReadOnly = false;
                    tbPostCode.ReadOnly = false;
                    tbPostCode.BackColor = Color.Cornsilk;
                    tbTelNo.ReadOnly = false;

                    break;

                default:
                    break;
            }

            tbDateOnSite.ReadOnly = true;
            tbDateCompleted.ReadOnly = true;

            SetButtonStates();
        }

        private void SetScreenAsDisplay()
        {
            txWorkID.Enabled = false;
            btSearchWO.Enabled = false;
            btSave.Enabled = false;

            tbJobNumber.ReadOnly = true;
            //tbDateIssued.ReadOnly = true;
            //tbDateRaised.ReadOnly = true;
            dtDateIssued.Enabled = false;
            dtDateRaised.Enabled = false;
            //tbRaisedBy.ReadOnly = true;
            //tbClerk.ReadOnly = true;
            cbClerk.Enabled = false;
            //tbPriority.ReadOnly = true;
            cbPriority.Enabled = false;
            tbStatus.ReadOnly = true;
            dtStatusDate.Enabled = false;
            tbComments.ReadOnly = true;

            tbCustomerName.ReadOnly = true;
            tbAddress1.ReadOnly = true;
            tbAddress2.ReadOnly = true;
            tbAddress3.ReadOnly = true;
            //tbAddress4.ReadOnly = true;
            //tbAddress5.ReadOnly = true;
            tbPostCode.ReadOnly = true;
            tbTelNo.ReadOnly = true;

            tbJobNumber.BackColor = Control.DefaultBackColor;
            cbClerk.BackColor = Control.DefaultBackColor;
            cbPriority.BackColor = Control.DefaultBackColor;
            tbCustomerName.BackColor = Control.DefaultBackColor;
            tbAddress1.BackColor = Control.DefaultBackColor;
            tbPostCode.BackColor = Control.DefaultBackColor;
        }

        protected override void OnShown(EventArgs e)
        {
            switch (screenMode)
            {
                case ScreenMode.Search:
                    txWorkID.Focus();
                    break;

                case ScreenMode.New:
                    tbJobNumber.Focus();
                    break;
            }
            base.OnShown(e);
        }

        private void btSearchWO_Click(object sender, EventArgs e)
        {
            jobNumber = txWorkID.Text;
            GetData(jobNumber);
        }

        private void GetData(String workOrderNumber)
        {
            gHasDateOnSite = false;
            gHasDateCompleted = false;

            try
            {
                DataTable dt = new DataTable();
                SqlCommand command = new SqlCommand("sp_WorkHead_Select", myConnection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@JobNumber", workOrderNumber));

                SqlDataReader reader = command.ExecuteReader();
                dt.Load(reader);

                if (dt.Rows.Count == 0)
                {
                    MessageBox.Show("Work Order " + workOrderNumber + " was not found.", "Work Orders Details", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                String strWorkID = dt.Rows[0]["WorkID"].ToString();
                if (!Int32.TryParse(strWorkID, out gWorkID))
                    gWorkID = -1;

                String strCustomerID = dt.Rows[0]["CustomerID"].ToString();
                if (!Int32.TryParse(strCustomerID, out gCustomerID))
                    gCustomerID = -1;

                tbJobNumber.Text = dt.Rows[0]["JobNumber"].ToString();
                dtDateRaised.Text = dt.Rows[0]["DateRaised"].ToString();
                dtDateIssued.Text = dt.Rows[0]["DateIssued"].ToString();
                //tbRaisedBy.Text = dt.Rows[0]["RaisedBy"].ToString();
                cbClerk.Text = dt.Rows[0]["ClerkName"].ToString();
                cbPriority.Text = dt.Rows[0]["PriorityDesc"].ToString();

                var dateOnSite = dt.Rows[0]["DateOnSite"];
                if (dateOnSite.GetType() == typeof(DateTime))
                {
                    gDateOnSite = (DateTime)dateOnSite;
                    gHasDateOnSite = true;
                    tbDateOnSite.Text = gDateOnSite.ToString("dd/MM/yyyy HH:mm");
                }


                var dateCompleted = dt.Rows[0]["DateCompleted"];
                if (dateCompleted.GetType() == typeof(DateTime))
                {
                    gDateCompleted = (DateTime)dateCompleted;
                    gHasDateCompleted = true;
                    tbDateCompleted.Text = gDateCompleted.ToString("dd/MM/yyyy HH:mm");
                }

                tbStatus.Text = dt.Rows[0]["StatusDesc"].ToString();
                dtStatusDate.Text = dt.Rows[0]["StatusDate"].ToString();
                tbComments.Text = dt.Rows[0]["Comments"].ToString();

                tbCustomerName.Text = dt.Rows[0]["CustomerName"].ToString();
                tbAddress1.Text = dt.Rows[0]["Address1"].ToString();
                tbAddress2.Text = dt.Rows[0]["Address2"].ToString();
                tbAddress3.Text = dt.Rows[0]["Address3"].ToString();
                //tbAddress4.Text = dt.Rows[0]["Address4"].ToString();
                //tbAddress5.Text = dt.Rows[0]["Address5"].ToString();
                tbPostCode.Text = dt.Rows[0]["PostCode"].ToString();
                tbTelNo.Text = dt.Rows[0]["TelNo"].ToString();

                gStatus = (int)dt.Rows[0]["StatusID"];

                // populate the grid
                PopulateWorkDetailGrid(gWorkID);
                PopulateWorkTotalsGrid(gWorkID);
                SetButtonStates();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

            return;
        }

        private void PopulateListboxes()
        {
            SqlCommand cmd;

            // populate the priority listbox
            cmd = new SqlCommand("sp_Listbox_Priority_Select", myConnection);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataReader rPriority = cmd.ExecuteReader();
            DataTable priorityData = new DataTable();
            priorityData.Load(rPriority);

            String defaultValue = "";
            foreach (DataRow row in priorityData.Rows)
                if (row["defflag"].ToString() == "True")
                    defaultValue = row["PriorityDesc"].ToString(); 

            cbPriority.DataSource = priorityData;
            cbPriority.DisplayMember = "PriorityDesc";

            if (defaultValue != "")
                cbPriority.SelectedIndex = cbPriority.FindString(defaultValue);

            // populate the clerks listbox
            cmd = new SqlCommand("sp_Listbox_Clerks_Select", myConnection);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataReader rClerk = cmd.ExecuteReader();
            DataTable clerkData = new DataTable();
            clerkData.Load(rClerk);

            cbClerk.DataSource = clerkData;
            cbClerk.DisplayMember = "Name";
        }

        public void ForceGridRefresh(Boolean refreshAll, string newJobNumber = null)
        {
            if (refreshAll)
            {
                if (newJobNumber != null)
                    jobNumber = newJobNumber;

                GetData(jobNumber);
            }
            else
            {
                PopulateWorkDetailGrid(gWorkID);
                PopulateWorkTotalsGrid(gWorkID);
            }
        }

        private void PopulateWorkDetailGrid(int workId)
        {
            // populate the grid
            SqlCommand command = new SqlCommand("sp_WorkDetail_Select", myConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("@WorkID", workId));

            SqlDataReader reader = command.ExecuteReader();
            DataTable gridData = new DataTable();
            gridData.Load(reader);

            gridWorkDetail.DataSource = new DataView(gridData);
            SetGridDetailsHeader();
            Globals.UpdateGridFont(gridWorkDetail);
        }

        private void PopulateWorkTotalsGrid(int workId)
        {
            // populate the grid
            SqlCommand command = new SqlCommand("sp_WorkDetail_Rate_Select", myConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("@WorkID", workId));

            SqlDataReader reader = command.ExecuteReader();
            DataTable gridData = new DataTable();
            gridData.Load(reader);

            gridWorkTotals.DataSource = new DataView(gridData);
            SetGridTotalsHeader();
            Globals.UpdateGridFont(gridWorkTotals);
        }

        private void SetGridDetailsHeader()
        {
            try
            {
                gridWorkDetail.Columns["WorkID"].Visible = false;
                gridWorkDetail.Columns["WorkLineID"].Visible = false;
                //gridWorkDetail.Columns["WorkDescription"].Visible = false;
                gridWorkDetail.Columns["Rated"].Visible = false;
                gridWorkDetail.Columns["WorkRate"].Visible = false;

                gridWorkDetail.Columns["WorkID"].HeaderText = "WorkID";
                gridWorkDetail.Columns["WorkLineID"].HeaderText = "WorkLineID";
                gridWorkDetail.Columns["Location"].HeaderText = "Location";
                gridWorkDetail.Columns["SurveyedUnits"].HeaderText = "Surveyed Units";
                gridWorkDetail.Columns["ActualUnits"].HeaderText = "Actual Units";
                gridWorkDetail.Columns["SOR"].HeaderText = "SOR";
                //gridWorkDetail.Columns["SurveyedTotal"].HeaderText = "Surveyed Total";
                //gridWorkDetail.Columns["ActualTotal"].HeaderText = "Actual Total";
                gridWorkDetail.Columns["WorkDescription"].HeaderText = "Non-SOR Job Description";
                //gridWorkDetail.Columns["WorkRate"].HeaderText = "Non-SOR Charge (£)";

                gridWorkDetail.Columns["Location"].Width = 130;
                gridWorkDetail.Columns["SurveyedUnits"].Width = 100;
                gridWorkDetail.Columns["ActualUnits"].Width = 100;
                gridWorkDetail.Columns["SOR"].Width = 500;
                //gridWorkDetail.Columns["SurveyedTotal"].Width = 100;
                //gridWorkDetail.Columns["ActualTotal"].Width = 100;
                gridWorkDetail.Columns["WorkDescription"].Width = 400;
                //gridWorkDetail.Columns["WorkRate"].Width = 100;


            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
            return;
        }

        private void SetGridTotalsHeader()
        {
            try
            {
                gridWorkTotals.Columns["WorkID"].Visible = false;

                gridWorkTotals.Columns["WorkID"].HeaderText = "WorkID";
                gridWorkTotals.Columns["SOR"].HeaderText = "SOR";
                gridWorkTotals.Columns["SurveyedUnits"].HeaderText = "Total Surveyed";
                gridWorkTotals.Columns["ActualUnits"].HeaderText = "Total Actual";
                gridWorkTotals.Columns["SurveyedTotal"].HeaderText = "Surveyed Total (£)";
                gridWorkTotals.Columns["ActualTotal"].HeaderText = "Actual Total (£)";
                gridWorkTotals.Columns["WorkDescription"].HeaderText = "Non-SOR Job Description";
                gridWorkTotals.Columns["WorkRate"].HeaderText = "Non-SOR Charge (£)";

                gridWorkTotals.Columns["SurveyedUnits"].Width = 100;
                gridWorkTotals.Columns["ActualUnits"].Width = 100;
                gridWorkTotals.Columns["SOR"].Width = 500;
                gridWorkTotals.Columns["SurveyedTotal"].Width = 100;
                gridWorkTotals.Columns["ActualTotal"].Width = 100;
                gridWorkTotals.Columns["WorkDescription"].Width = 400;
                gridWorkTotals.Columns["WorkRate"].Width = 100;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
            return;
        }

        private void SetButtonStates()
        {
            btNew.Enabled = (gStatus >= 0 && gStatus < 99 && editing == EditField.None);
            btAmend.Enabled = (gStatus >= 0 && gStatus < 99 && editing == EditField.None);
            btDelete.Enabled = (gStatus >= 0 && gStatus < 99 && editing == EditField.None);
            btHistory.Enabled = (gStatus >= 0 && gStatus <= 99 && editing == EditField.None);
            btStatus.Enabled = (gStatus >= 0 && gStatus <= 99 && editing == EditField.None);

            btEditJobNumber.Enabled = (gStatus >= 0 && gStatus <= 99 && editing == EditField.None);
            btEditDateIssued.Enabled = (gStatus >= 0 && gStatus <= 99 && editing == EditField.None);
            btEditDateRaised.Enabled = (gStatus >= 0 && gStatus <= 99 && editing == EditField.None);
            btEditPriority.Enabled = (gStatus >= 0 && gStatus <= 99 && editing == EditField.None);
            btEditClerk.Enabled = (gStatus >= 0 && gStatus <= 99 && editing == EditField.None);
            btEditComment.Enabled = (gStatus >= 0 && gStatus <= 99 && editing == EditField.None);
            btEditDateOnSite.Enabled = (gStatus >= 0 && gStatus <= 99 && editing == EditField.None);
            btEditDateCompleted.Enabled = (gStatus >= 0 && gStatus <= 99 && editing == EditField.None);
            btEditCustomer.Enabled = (gStatus >= 0 && gStatus <= 99 && (editing == EditField.None || editing == EditField.Customer));

            btClose.Enabled = true;
            return;
        }

        private void Form1_ResizeEnd(object sender, EventArgs e)
        {
            //
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            Form frm = sender as Form;
            SetScreen(frm);
        }

        private void SetScreen(Form frm)
        {
            groupBox4.Width = frm.ClientSize.Width - groupBox4.Left - 12;
            tbComments.Width = groupBox4.Width - tbComments.Left - btEditComment.Width - 20;
            btEditComment.Left = tbComments.Left + tbComments.Width + 10;

            btNew.Top = frm.ClientSize.Height - btNew.ClientSize.Height - 6;
            btDelete.Top = btNew.Top;
            btAmend.Top = btNew.Top;
            btClose.Top = btNew.Top;

            int gridTotalHeight = btNew.Top - (groupBox4.Top + groupBox4.Height + 24);

            groupBox3.Width = frm.ClientSize.Width - groupBox3.Left - 12;
            groupBox3.Height = gridTotalHeight / 2; //frm.ClientSize.Height - groupBox3.Top - btNew.Height - 10;

            groupBox5.Width = frm.ClientSize.Width - groupBox5.Left - 12;
            //groupBox5.Height = frm.ClientSize.Height - groupBox3.Top - btNew.Height - 10;
            groupBox5.Height = gridTotalHeight / 2;
            groupBox5.Top = groupBox3.Top + groupBox3.Height +12;

            gridWorkDetail.Width = groupBox3.Width - gridWorkDetail.Left - 10;
            gridWorkDetail.Height = groupBox3.Height - gridWorkDetail.Top - 10;

            gridWorkTotals.Width = groupBox5.Width - gridWorkTotals.Left - 10;
            gridWorkTotals.Height = groupBox5.Height - gridWorkTotals.Top - 10;
        }

        private int CreateWorkOrderHeader(
            String jobNumber,
            DateTime dateRaised,
            DateTime dateIssued,
            int clerkId,
            int priority,
            int statusId,
            int customerID)
        {
            int iWorkOrderID = -1;

            try
            {
                // Set uyp the command object
                SqlCommand command = new SqlCommand("sp_WorkHead_Insert", myConnection);
                command.CommandType = CommandType.StoredProcedure;

                // Add all parmeters including the output parameters here
                command.Parameters.Add(new SqlParameter("@JobNumber", jobNumber));
                command.Parameters.Add(new SqlParameter("@DateRaised", dateRaised));
                command.Parameters.Add(new SqlParameter("@DateIssued", dateIssued));
                //command.Parameters.Add(new SqlParameter("@RaisedBy", raisedBy));
                command.Parameters.Add(new SqlParameter("@ClerkID", clerkId));
                command.Parameters.Add(new SqlParameter("@Priority", priority));
                command.Parameters.Add(new SqlParameter("@StatusID", statusId));
                command.Parameters.Add(new SqlParameter("@CustomerID", customerID));

                SqlParameter parCustomerID = command.Parameters.Add("@WorkOrderID", SqlDbType.Int);
                parCustomerID.Direction = ParameterDirection.Output;

                SqlParameter parRetval = command.Parameters.Add("@retval", SqlDbType.Int);
                parRetval.Direction = ParameterDirection.ReturnValue;

                // Execute the SP
                command.ExecuteNonQuery();

                int retval = (int)command.Parameters["@retval"].Value;

                //  0 is success
                if (retval == 0)
                    iWorkOrderID = (int)command.Parameters["@WorkOrderID"].Value;
                else
                    iWorkOrderID = -1;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
                iWorkOrderID = -1;
            }

            return iWorkOrderID;
        }

        private int CreateCustomer(
            String customerName, 
            String address1, 
            String address2, 
            String address3, 
            String address4, 
            String address5, 
            String postCode, 
            String telNo)
        {
            int iCustomerID = -1;

            try
            {

                // Set uyp the command object
                SqlCommand command = new SqlCommand("sp_Customer_Insert", myConnection);
                command.CommandType = CommandType.StoredProcedure;

                // Add all parmeters including the output parameters here
                command.Parameters.Add(new SqlParameter("@CustomerName", customerName));
                command.Parameters.Add(new SqlParameter("@Address1", address1));
                command.Parameters.Add(new SqlParameter("@Address2", address2));
                command.Parameters.Add(new SqlParameter("@Address3", address3));
                command.Parameters.Add(new SqlParameter("@Address4", address4));
                command.Parameters.Add(new SqlParameter("@Address5", address5));
                command.Parameters.Add(new SqlParameter("@PostCode", postCode));
                command.Parameters.Add(new SqlParameter("@TelNo", telNo));

                SqlParameter parCustomerID = command.Parameters.Add("@CustomerID", SqlDbType.Int);
                parCustomerID.Direction = ParameterDirection.Output;

                SqlParameter parRetval = command.Parameters.Add("@retval", SqlDbType.Int);
                parRetval.Direction = ParameterDirection.ReturnValue;

                // Execute the SP
                command.ExecuteNonQuery();

                int retval = (int)command.Parameters["@retval"].Value;

                //  0 is success
                if (retval == 0)
                    iCustomerID = (int)command.Parameters["@CustomerID"].Value;
                else
                    iCustomerID = -1;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
                iCustomerID = -1;
            }


            return iCustomerID;
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            // TO DO: Add data validation here
            long tmpJobNumber = -1;

            if (tbJobNumber.Text.Length == 0)
            {
                MessageBox.Show("The Work Order number must be entered.", "Work Order", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else
            {
                if (!long.TryParse(tbJobNumber.Text, out tmpJobNumber))
                {
                    MessageBox.Show("The Work Order number must be a number.", "Work Order", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (tmpJobNumber <= 0)
                {
                    MessageBox.Show("The Work Order number must be greater than 0.", "Work Order", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (tmpJobNumber > 2147483647)
                {
                    MessageBox.Show("The Work Order number is too big.", "Work Order", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }

            // cache the job number
            jobNumber = tbJobNumber.Text;

            if (tbCustomerName.Text.Length == 0)
            {
                MessageBox.Show("The customer name must be entered.", "Work Order", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (tbAddress1.Text.Length == 0)
            {
                MessageBox.Show("The first line of the address must be entered.", "Work Order", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (tbPostCode.Text.Length == 0)
            {
                MessageBox.Show("The post code must be entered.", "Work Order", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            try
            {

                int retval = ValidateAgainstDB(jobNumber);

                // see validate SP for meaning of return values
                if (retval == -1)
                {
                    MessageBox.Show("The Work Order number entered is already in use.", "Work Order", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                // Create the customer
                int customerID = CreateCustomer(tbCustomerName.Text,
                                                tbAddress1.Text,
                                                tbAddress2.Text,
                                                tbAddress3.Text,
                                                "",
                                                "",
                                                tbPostCode.Text,
                                                tbTelNo.Text);

                if (customerID != -1)
                {
                    // Get the priority from the list box
                    int priority = 1;

                    // It was populated with data table hence the selected value is a data row
                    DataRowView rPriority = (DataRowView)cbPriority.SelectedValue;
                    
                    // Retreive as string as this is the most 'generic' data type
                    string tmpPriorty = rPriority.Row["Priority"].ToString();

                    //Convert to integer if possible
                    if (!int.TryParse(tmpPriorty, out priority))
                    {
                        MessageBox.Show("Failed to convert priority from list.", "Work Order", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    int clerkId = 1;

                    // It was populated with data table hence the selected value is a data row
                    DataRowView rClerk = (DataRowView)cbClerk.SelectedValue;

                    // Retreive as string as this is the most 'generic' data type
                    string tmpClerk = rClerk.Row["ClerkID"].ToString();

                    //Convert to integer if possible
                    if (!int.TryParse(tmpClerk, out clerkId))
                    {
                        MessageBox.Show("Failed to convert clerk from list.", "Work Order", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }

                    // Create the work order
                    int retWorkID = CreateWorkOrderHeader(tbJobNumber.Text,
                                                          dtDateRaised.Value,
                                                          dtDateIssued.Value,
                                                          clerkId,
                                                          priority,
                                                          10,                       // This magic number is an open/received status
                                                          customerID);

                    // If not failure
                    if (retWorkID != -1)
                    {
                        // Set up the screen data entry
                        SetScreenAsDisplay();

                        // repopulate the screen
                        GetData(tbJobNumber.Text);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private int ValidateAgainstDB(String jobNumber)
        {
            // Check work order does not already exists
            SqlCommand command = new SqlCommand("sp_WorkHead_Validate", myConnection);
            command.CommandType = CommandType.StoredProcedure;

            // Add all parmeters including the output parameters here
            command.Parameters.Add(new SqlParameter("@JobNumber", jobNumber));

            SqlParameter parRetval = command.Parameters.Add("@retval", SqlDbType.Int);
            parRetval.Direction = ParameterDirection.ReturnValue;

            // Execute the SP
            command.ExecuteNonQuery();

            int retval = (int)command.Parameters["@retval"].Value;
            return retval;
        }

        private void btAmend_Click(object sender, EventArgs e)
        {
            var row = gridWorkDetail.SelectedRows[0].Index;

            int workId = (int)gridWorkDetail.Rows[row].Cells["WorkID"].Value;
            int workLineId = (int)gridWorkDetail.Rows[row].Cells["WorkLineID"].Value;

            var jobDetail = new JobDetail(this, myConnection, JobDetailScreenMode.Amend, workId, workLineId);
            jobDetail.ShowDialog(this);
        }

        private void btDelete_Click(object sender, EventArgs e)
        {
            DialogResult retval = MessageBox.Show("Are you sure you want to delete a job from this work order?", "Work Order", MessageBoxButtons.YesNo);
            if (retval == DialogResult.Yes)
            {
                var row = gridWorkDetail.SelectedRows[0].Index;

                int workId = (int)gridWorkDetail.Rows[row].Cells["WorkID"].Value;
                int workLineId = (int)gridWorkDetail.Rows[row].Cells["WorkLineID"].Value;

                // Perform the delete
                DeleteWorkDetail(workId, workLineId);

                // populate the grid
                PopulateWorkDetailGrid(gWorkID);
                PopulateWorkTotalsGrid(gWorkID);
            }
        }

        private void btNew_Click(object sender, EventArgs e)
        {
            var jobDetail = new JobDetail(this, myConnection, JobDetailScreenMode.Create, gWorkID, -1);
            jobDetail.ShowDialog(this);
        }

        private int DeleteWorkDetail(int WorkID, int WorkLineID)
        {
            int retval = -1;

            try
            {
                // Set uyp the command object
                SqlCommand command = new SqlCommand("sp_WorkDetail_Delete", myConnection);
                command.CommandType = CommandType.StoredProcedure;

                // Add all parmeters including the output parameters here
                command.Parameters.Add(new SqlParameter("@WorkID", WorkID));
                command.Parameters.Add(new SqlParameter("@WorkLineID", WorkLineID));

                SqlParameter parRetval = command.Parameters.Add("@retval", SqlDbType.Int);
                parRetval.Direction = ParameterDirection.ReturnValue;

                // Execute the SP
                command.ExecuteNonQuery();

                retval = (int)command.Parameters["@retval"].Value;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
                retval = -1;
            }

            return retval;
        }

        private void WorkOrderDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (myParent.GetType() == typeof(Main))
            {
                Globals.RestoreScreen(myParent);

                var m = myParent as Main;
                //if (screenMode == ScreenMode.New)
                    m.RestoreScreen(true);
                //else
                //    m.RestoreScreen(false);
            }
            //else if (myParent.GetType() == typeof(Home))
            //{
            //    Home m = myParent as Home;
            //    m.C
            //}
        }

        private void btHistory_Click(object sender, EventArgs e)
        {
            var history = new WorkHistory(this, myConnection, gWorkID);
            history.MdiParent = Globals.MDIParentForm;
            history.Show();
            Globals.HideScreen(this);
        }

        private void txWorkID_KeyDown(object sender, KeyEventArgs e)
        {
            TextBox txt = sender as TextBox;
            if (e.KeyCode == Keys.Enter && txt.TextLength > 0)
            {
                jobNumber = txt.Text;
                GetData(jobNumber);
            }
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btStatus_Click(object sender, EventArgs e)
        {
            if (gWorkID <= 0)
                return;

            var cs = new ChangeStatus(this, myConnection, gWorkID, tbStatus.Text, tbComments.Text);
            cs.MdiParent = Globals.MDIParentForm;
            cs.Show();
            Globals.HideScreen(this);
            cs.WindowState = FormWindowState.Normal;
        }

        private void gridWorkDetail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                PopulateWorkDetailGrid(gWorkID);
            }
        }

        private void gridWorkTotals_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                PopulateWorkTotalsGrid(gWorkID);
            }
        }

        private void btEditDateRasied_Click(object sender, EventArgs e)
        {
            if (gWorkID <= 0)
                return;

            var changeDate = new ChangeDate(this, myConnection, gWorkID, dtDateRaised.Value, "DateRaised");
            changeDate.MdiParent = Globals.MDIParentForm;
            changeDate.Show();
            Globals.HideScreen(this);
        }

        private void btEditDateIssued_Click(object sender, EventArgs e)
        {
            if (gWorkID <= 0)
                return;

            var changeDate = new ChangeDate(this, myConnection, gWorkID, dtDateIssued.Value, "DateIssued");
            changeDate.MdiParent = Globals.MDIParentForm;
            changeDate.Show();
            Globals.HideScreen(this);
        }

        private void btEditComment_Click(object sender, EventArgs e)
        {
            if (gWorkID <= 0)
                return;

            var cs = new ChangeComment(this, myConnection, gWorkID, tbComments.Text);
            cs.MdiParent = Globals.MDIParentForm;
            cs.Show();
            Globals.HideScreen(this);
        }

        private void btEditClerk_Click(object sender, EventArgs e)
        {
            if (gWorkID <= 0)
                return;

            var change = new ChangeClerk(this, myConnection, gWorkID, cbClerk.Text);
            change.MdiParent = Globals.MDIParentForm;
            change.Show();
            Globals.HideScreen(this);
        }

        private void btEditPriority_Click(object sender, EventArgs e)
        {
            if (gWorkID <= 0)
                return;

            var change = new ChangePriority(this, myConnection, gWorkID, cbPriority.Text);
            change.MdiParent = Globals.MDIParentForm;
            change.Show();
            Globals.HideScreen(this);

            //// This is work in progress
            //if (editing == EditField.None)
            //{
            //    btEditPriority.Text = "Save";
            //    editing = EditField.Priority;

            //    // Put the form details into edit mode
            //    cbPriority.Enabled = true;
            //    cbPriority.BackColor = Color.Cornsilk;
            //}
            //else
            //{
            //    btEditPriority.Text = "Edit";
            //    editing = EditField.None;

            //    // Come out of edit mode
            //    SetScreenAsDisplay();

            //    try
            //    {
            //        // Get the priority from the list box
            //        int priority = 1;

            //        // It was populated with data table hence the selected value is a data row
            //        DataRowView rPriority = (DataRowView)cbPriority.SelectedValue;

            //        // Retreive as string as this is the most 'generic' data type
            //        string tmpPriorty = rPriority.Row["Priority"].ToString();

            //        //Convert to integer if possible
            //        if (!int.TryParse(tmpPriorty, out priority))
            //        {
            //            MessageBox.Show("Failed to convert priority from list.", "Work Order", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //            return;
            //        }

            //        // Set up the command object
            //        SqlCommand command = new SqlCommand("sp_WorkHead_Update_Priority", myConnection);
            //        command.CommandType = CommandType.StoredProcedure;

            //        // Add all parmeters including the output parameters here
            //        command.Parameters.Add(new SqlParameter("@WorkID", gWorkID));
            //        command.Parameters.Add(new SqlParameter("@Priority", priority));

            //        SqlParameter parRetval = command.Parameters.Add("@retval", SqlDbType.Int);
            //        parRetval.Direction = ParameterDirection.ReturnValue;

            //        // Execute the SP
            //        command.ExecuteNonQuery();

            //        int retval = (int)command.Parameters["@retval"].Value;

            //        if (retval != 0)
            //        {
            //            MessageBox.Show("Failed to update the job priority.", "Work Order", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //            GetData(jobNumber);
            //        }
            //    }
            //    catch (Exception err)
            //    {
            //        MessageBox.Show(err.Message);
            //        GetData(jobNumber);
            //    }
            //}
        }

        private void btEditDateOnSite_Click(object sender, EventArgs e)
        {
            if (gWorkID <= 0)
                return;
            DateTime theDate;
            if (gHasDateOnSite)
                theDate = gDateOnSite;
            else
                theDate = DateTime.Now;

            var change = new ChangeDate(this, myConnection, gWorkID, theDate, "DateOnSite");
            change.MdiParent = Globals.MDIParentForm;
            change.Show();
            Globals.HideScreen(this);
        }

        private void btEditDateCompleted_Click(object sender, EventArgs e)
        {
            if (gWorkID <= 0)
                return;

            DateTime theDate;
            if (gHasDateOnSite)
                theDate = gDateCompleted;
            else
                theDate = DateTime.Now;

            var change = new ChangeDate(this, myConnection, gWorkID, theDate, "DateCompleted");
            change.MdiParent = Globals.MDIParentForm;
            change.Show();
            Globals.HideScreen(this);
        }

        private void btEditJobNumber_Click(object sender, EventArgs e)
        {
            if (gWorkID <= 0)
                return;

            var cj = new ChangeJobNumber(this, myConnection, gWorkID, tbJobNumber.Text);
            cj.MdiParent = Globals.MDIParentForm;
            cj.Show();
            Globals.HideScreen(this);
        }

        private void btEditCustomer_Click(object sender, EventArgs e)
        {
            if (gCustomerID <= 0)
                return;

            if (editing == EditField.None)
            {
                btEditCustomer.Text = "Save";
                editing = EditField.Customer;

                // Put the customer details into edit mode
                tbCustomerName.ReadOnly = false;
                tbCustomerName.BackColor = Color.Cornsilk;
                tbAddress1.ReadOnly = false;
                tbAddress1.BackColor = Color.Cornsilk;
                tbAddress2.ReadOnly = false;
                tbAddress3.ReadOnly = false;
                tbPostCode.ReadOnly = false;
                tbPostCode.BackColor = Color.Cornsilk;
                tbTelNo.ReadOnly = false;
            }
            else
            {
                btEditCustomer.Text = "Edit";
                editing = EditField.None;

                // Come out of edit mode
                SetScreenAsDisplay();

                try
                {
                    // save the customer details, set up the command object
                    SqlCommand command = new SqlCommand("sp_Customer_Update", myConnection);
                    command.CommandType = CommandType.StoredProcedure;

                    // Add all parmeters including the output parameters here
                    command.Parameters.Add(new SqlParameter("@CustomerID", gCustomerID));
                    command.Parameters.Add(new SqlParameter("@CustomerName", tbCustomerName.Text));
                    command.Parameters.Add(new SqlParameter("@Address1", tbAddress1.Text));
                    command.Parameters.Add(new SqlParameter("@Address2", tbAddress2.Text));
                    command.Parameters.Add(new SqlParameter("@Address3", tbAddress3.Text));
                    command.Parameters.Add(new SqlParameter("@Address4", ""));
                    command.Parameters.Add(new SqlParameter("@Address5", ""));
                    command.Parameters.Add(new SqlParameter("@PostCode", tbPostCode.Text));
                    command.Parameters.Add(new SqlParameter("@TelNo", tbTelNo.Text));

                    SqlParameter parRetval = command.Parameters.Add("@retval", SqlDbType.Int);
                    parRetval.Direction = ParameterDirection.ReturnValue;

                    // Execute the SP
                    command.ExecuteNonQuery();

                    int retval = (int)command.Parameters["@retval"].Value;

                    //  0 is success
                    if (retval != 0)
                    {
                        MessageBox.Show("Failed to update the customer details.", "Work Order", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        GetData(jobNumber);
                    }
                }
                catch (Exception err)
                {
                    MessageBox.Show(err.Message);
                    GetData(jobNumber);
                }
            }

            SetButtonStates();
        }

        private void WorkOrderDetail_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape && editing != EditField.None)
            {
                editing = EditField.None;
                SetScreenAsDisplay();
            }
        }
    }
}
