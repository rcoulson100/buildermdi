﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BuilderMDI
{
    public partial class Customers : Form
    {
        SqlConnection myConnection;
        DataTable dtSelected;
        Form myParent;

        public Customers(Form p, SqlConnection cnn)
        {
            InitializeComponent();
            this.Icon = BuilderMDI.Properties.Resources.ICON;
            myConnection = cnn;
            myParent = p;
        }

        private void Customers_Load(object sender, EventArgs e)
        {
            Form frm = sender as Form;
            frm.Size = new Size(900, 600);
            SetScreen(frm);
        }

        private void SetScreen(Form frm)
        {
            if (frm.Width <= 700)
                frm.Width = 700;

            groupBox1.Left = 5;
            groupBox1.Top = 5;
            groupBox1.Width = frm.ClientSize.Width - groupBox1.Left - 5;
            groupBox1.Height = frm.ClientSize.Height - groupBox1.Top - btClose.ClientSize.Height - 10;

            gridData.Left = 5;
            gridData.Top = 12;

            gridData.Width = groupBox1.Width - (gridData.Left * 2);
            gridData.Height = groupBox1.Height - gridData.Top - 10;

            btClose.Left = 5;
            btClose.Top = frm.ClientSize.Height - btClose.ClientSize.Height - 5;
        }

        private void Customers_Shown(object sender, EventArgs e)
        {
            PopulateUsersGrid();
            //SetButtonStates();
        }

        private void PopulateUsersGrid()
        {
            try
            {
                int row = -1;

                if (gridData.SelectedRows.Count == 1)
                    row = gridData.SelectedRows[0].Index;

                SqlCommand command = new SqlCommand("sp_Customers_Select", myConnection);
                command.CommandType = CommandType.StoredProcedure;

                // populate the grid
                SqlDataReader reader = command.ExecuteReader();
                dtSelected = new DataTable();
                dtSelected.Load(reader);

                gridData.DataSource = new DataView(dtSelected);
                SetGridHeader();
                Globals.UpdateGridFont(gridData);

                // re-select the row
                if (row != -1)
                {
                    if (row > gridData.RowCount)
                        row = gridData.RowCount;

                    gridData.CurrentCell = gridData.Rows[row].Cells[1];
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }

        private void SetGridHeader()
        {
            try
            {
                gridData.Columns["CustomerID"].Visible = false;
                gridData.Columns["Address4"].Visible = false;
                gridData.Columns["Address5"].Visible = false;

                gridData.Columns["CustomerName"].HeaderText = "Name";
                gridData.Columns["Address1"].HeaderText = "Address 1";
                gridData.Columns["Address2"].HeaderText = "Address 2";
                gridData.Columns["Address3"].HeaderText = "Address 3";
                gridData.Columns["PostCode"].HeaderText = "Post Code";
                gridData.Columns["TelNo"].HeaderText = "Tel No.";

                gridData.Columns["CustomerName"].Width = 150;
                gridData.Columns["Address1"].Width = 150;
                gridData.Columns["Address2"].Width = 150;
                gridData.Columns["Address3"].Width = 150;
                gridData.Columns["PostCode"].Width = 80;
                gridData.Columns["TelNo"].Width = 80;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
            return;
        }

        private void Customers_Resize(object sender, EventArgs e)
        {
            Form frm = sender as Form;
            SetScreen(frm);
        }

        private void Customers_FormClosed(object sender, FormClosedEventArgs e)
        {
            //if (myParent.GetType() == typeof(Main))
            //{
            //    Globals.RestoreScreen(myParent);
            //}
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gridData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                PopulateUsersGrid();
            }
        }
    }
}
