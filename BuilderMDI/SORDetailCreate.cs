﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BuilderMDI
{
    public partial class SORDetailCreate : Form
    {
        public enum ScreenMode
        {
            Create,
            Edit
        }

        SqlConnection myConnection;
        ScreenMode myMode;
        Form myParent;
        int mySORID = -1;
        int myBandID = -1;
        String myCurrentDesc = "";
        double myMinUnit = 0;
        double myMaxUnit = 0;
        double myRate = 0;


        public SORDetailCreate(Form p, SqlConnection cnn, ScreenMode mode, int sorID = -1, int bandId = -1, String description = "", double minUnit = 0, double maxUnit = 0, double rate = 0)
        {
            InitializeComponent();
            this.Icon = BuilderMDI.Properties.Resources.ICON;
            myConnection = cnn;
            myMode = mode;
            myParent = p;
            mySORID = sorID;
            myCurrentDesc = description;
            myMinUnit = minUnit;
            myMaxUnit = maxUnit;
            myRate = rate;
            myBandID = bandId;
        }

        private void SORDetailCreate_Load(object sender, EventArgs e)
        {
            Form frm = sender as Form;

            tbDescription.BackColor = Color.Cornsilk;
            tbMinUnit.BackColor = Color.Cornsilk;
            tbMaxUnit.BackColor = Color.Cornsilk;
            tbRate.BackColor = Color.Cornsilk;

            switch (myMode)
            {
                case ScreenMode.Create:
                    this.Text = "SOR Band Create";
                    break;

                case ScreenMode.Edit:
                    this.Text = "SOR Band Edit";
                    tbDescription.Text = myCurrentDesc;
                    tbMinUnit.Text = myMinUnit.ToString();
                    tbMaxUnit.Text = myMaxUnit.ToString();
                    tbRate.Text = myRate.ToString();
                    break;

                default:
                    break;
            }

            SetScreen(frm);
        }

        private void SetScreen(Form frm)
        {
            //groupBox1.Left = 5;
            //groupBox1.Top = 5;
            //groupBox1.Width = frm.ClientSize.Width - groupBox1.Left - 5;
            //groupBox1.Height = frm.ClientSize.Height - groupBox1.Top - btOK.Height - 10;

            //btOK.Left = 5;
            //btOK.Top = frm.Height - btOK.Height - 45;
        }

        private void SetButtonStates()
        {
            btOK.Enabled = true;
            return;
        }

        private void SORDetailCreate_Shown(object sender, EventArgs e)
        {
            SetButtonStates();
        }

        protected override void OnShown(EventArgs e)
        {
            tbDescription.Focus();
            base.OnShown(e);
        }

        private void SORDetailCreate_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (myParent.GetType() == typeof(SORDetail))
            {
                SORDetail s = myParent as SORDetail;
                Globals.RestoreScreen(myParent);
                s.ForceRefresh();
            }
        }

        private void btOK_Click(object sender, EventArgs e)
        {
            if (tbDescription.Text.Trim().Length == 0)
            {
                MessageBox.Show("SOR Description must be entered.", "SOR Band", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (tbDescription.Text.Trim().Length > 10)
            {
                MessageBox.Show("SOR Description is too long. Please use 10 characters or less.", "SOR Band", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (mySORID == -1)
            {
                MessageBox.Show("System error. Screen is in edit mode but the SOR ID has not been set.", "SOR Band", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int minUnit;
            if (!int.TryParse(tbMinUnit.Text, out minUnit))
            {
                MessageBox.Show("The Minimum Units field must be numeric.", "SOR Band", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            int maxUnit;
            if (!int.TryParse(tbMaxUnit.Text, out maxUnit))
            {
                MessageBox.Show("The Maximum Units field must be numeric.", "SOR Band", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            double rate;
            if (!double.TryParse(tbRate.Text, out rate))
            {
                MessageBox.Show("The Rate field must be numeric.", "SOR Band", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (myMode == ScreenMode.Create)
            {
                if (DoCreate(mySORID, tbDescription.Text.Trim(), minUnit, maxUnit, rate) == 0)
                {
                    this.Close();
                }
            }
            else
            {
                if (myBandID == -1)
                {
                    MessageBox.Show("System error. Screen is in edit mode but the Band ID has not been set.", "SOR Band", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                if (DoEdit(mySORID, myBandID, tbDescription.Text.Trim(), minUnit, maxUnit, rate) == 0)
                {
                    this.Close();
                }
            }
        }

        private int DoCreate(int sorID, String sorDesc, int minUnit, int maxUnit, double rate)
        {
            int retval = -1;

            try
            {
                // Set uyp the command object
                SqlCommand command = new SqlCommand("sp_SORBands_Insert", myConnection);
                command.CommandType = CommandType.StoredProcedure;

                // Add all parmeters including the output parameters here
                command.Parameters.Add(new SqlParameter("@SORID", sorID));
                command.Parameters.Add(new SqlParameter("@Band", sorDesc));
                command.Parameters.Add(new SqlParameter("@Min", minUnit));
                command.Parameters.Add(new SqlParameter("@Max", maxUnit));
                command.Parameters.Add(new SqlParameter("@Rate", rate));

                SqlParameter parRetval = command.Parameters.Add("@retval", SqlDbType.Int);
                parRetval.Direction = ParameterDirection.ReturnValue;

                // Execute the SP
                command.ExecuteNonQuery();

                retval = (int)command.Parameters["@retval"].Value;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
                retval = -1;
            }

            return retval;
        }

        private int DoEdit(int sorID, int bandID, String sorDesc, int minUnit, int maxUnit, double rate)
        {
            int retval = -1;

            try
            {
                // Set uyp the command object
                SqlCommand command = new SqlCommand("sp_SORBands_Update", myConnection);
                command.CommandType = CommandType.StoredProcedure;

                // Add all parmeters including the output parameters here
                command.Parameters.Add(new SqlParameter("@SORID", sorID));
                command.Parameters.Add(new SqlParameter("@BandID", bandID));
                command.Parameters.Add(new SqlParameter("@Band", sorDesc));
                command.Parameters.Add(new SqlParameter("@Min", minUnit));
                command.Parameters.Add(new SqlParameter("@Max", maxUnit));
                command.Parameters.Add(new SqlParameter("@Rate", rate));

                SqlParameter parRetval = command.Parameters.Add("@retval", SqlDbType.Int);
                parRetval.Direction = ParameterDirection.ReturnValue;

                // Execute the SP
                command.ExecuteNonQuery();

                retval = (int)command.Parameters["@retval"].Value;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
                retval = -1;
            }

            return retval;
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
