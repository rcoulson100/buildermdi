﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BuilderMDI
{
    public partial class SORDetail : Form
    {
        SqlConnection myConnection;
        DataTable dtSelected;
        Form myParent;
        int mySorID;

        public SORDetail(Form p, SqlConnection cnn, int sorId)
        {
            InitializeComponent();
            this.Icon = BuilderMDI.Properties.Resources.ICON;
            myConnection = cnn;
            myParent = p;
            mySorID = sorId;
        }

        private void SORDetail_Load(object sender, EventArgs e)
        {
            Form frm = sender as Form;
            frm.Size = new Size(800, 500);
            SetScreen(frm);
        }

        private void SetScreen(Form frm)
        {
            if (frm.Width <= 900)
                frm.Width = 900;

            groupBox1.Left = 5;
            groupBox1.Top = 5;
            groupBox1.Width = frm.ClientSize.Width - groupBox1.Left - 5;
            groupBox1.Height = frm.ClientSize.Height - groupBox1.Top - btNew.ClientSize.Height - 10;

            gridData.Left = 5;
            gridData.Top = 12;

            gridData.Width = groupBox1.Width - (gridData.Left * 2);
            gridData.Height = groupBox1.Height - gridData.Top - 10;

            btNew.Left = 5;
            btNew.Top = frm.ClientSize.Height - btNew.ClientSize.Height - 5;
            btChange.Left = btNew.Left + btNew.ClientSize.Width + 10;
            btChange.Top = btNew.Top;
            btDelete.Left = btChange.Left + btChange.ClientSize.Width + 10;
            btDelete.Top = btNew.Top;
            btClose.Left = btDelete.Left + btDelete.ClientSize.Width + 10;
            btClose.Top = btNew.Top;
        }

        private void SORDetail_Shown(object sender, EventArgs e)
        {
            PopulateGrid();
            //SetButtonStates();
        }

        public void ForceRefresh()
        {
            PopulateGrid();
        }

        private void PopulateGrid()
        {
            SqlCommand command = new SqlCommand("sp_SORBands_Select", myConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add(new SqlParameter("@SorID", mySorID));

            // populate the grid
            SqlDataReader reader = command.ExecuteReader();
            dtSelected = new DataTable();
            dtSelected.Load(reader);

            gridData.DataSource = new DataView(dtSelected);
            SetGridHeader();
            Globals.UpdateGridFont(gridData);
        }

        private void SetGridHeader()
        {
            try
            {
                gridData.Columns["SORID"].Visible = false;
                gridData.Columns["BandID"].Visible = false;

                gridData.Columns["Band"].HeaderText = "SOR Band";
                gridData.Columns["Min"].HeaderText = "Min Quantity";
                gridData.Columns["Max"].HeaderText = "Max Quantity";
                gridData.Columns["Rate"].HeaderText = "Rate (£)";

                gridData.Columns["Band"].Width = 200;
                gridData.Columns["Min"].Width = 150;
                gridData.Columns["Max"].Width = 150;
                gridData.Columns["Rate"].Width = 80;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
            return;
        }

        private void SORDetail_Resize(object sender, EventArgs e)
        {
            Form frm = sender as Form;
            SetScreen(frm);
        }

        private void SORDetail_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (myParent.GetType() == typeof(SOR))
            {
                Globals.RestoreScreen(myParent);
            }
        }

        private void btDelete_Click(object sender, EventArgs e)
        {
            if (DialogResult.No == MessageBox.Show("Are you sure?", "SOR Band", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                return;

            if (gridData.SelectedRows.Count == 0)
                return;

            var row = gridData.SelectedRows[0].Index;

            int sorID = (int)gridData.Rows[row].Cells["SORID"].Value;
            int bandID = (int)gridData.Rows[row].Cells["BandID"].Value;

            if (DoDelete(sorID, bandID) == 0)
                PopulateGrid();
        }

        private int DoDelete(int sorID, int bandID)
        {
            int retval = -1;

            try
            {
                // Set uyp the command object
                SqlCommand command = new SqlCommand("sp_SORBands_Delete", myConnection);
                command.CommandType = CommandType.StoredProcedure;

                // Add all parmeters including the output parameters here
                command.Parameters.Add(new SqlParameter("@SORID", sorID));
                command.Parameters.Add(new SqlParameter("@BandID", bandID));

                SqlParameter parRetval = command.Parameters.Add("@retval", SqlDbType.Int);
                parRetval.Direction = ParameterDirection.ReturnValue;

                // Execute the SP
                command.ExecuteNonQuery();

                retval = (int)command.Parameters["@retval"].Value;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
                retval = -1;
            }

            return retval;
        }

        private void btNew_Click(object sender, EventArgs e)
        {
            SORDetailCreate cre = new SORDetailCreate(this, myConnection, SORDetailCreate.ScreenMode.Create, mySorID);
            cre.MdiParent = Globals.MDIParentForm;
            cre.Show();
            cre.WindowState = FormWindowState.Normal;
            Globals.HideScreen(this);
        }

        private void btChange_Click(object sender, EventArgs e)
        {
            try
            {
                var row = gridData.SelectedRows[0].Index;
                int bandID = (int)gridData.Rows[row].Cells["BandID"].Value;
                String band = gridData.Rows[row].Cells["Band"].Value.ToString();
                String tmpMinUnit = gridData.Rows[row].Cells["Min"].Value.ToString();
                String tmpMaxUnit = gridData.Rows[row].Cells["Max"].Value.ToString();
                String tmpRate = gridData.Rows[row].Cells["Rate"].Value.ToString();

                double rate;
                double.TryParse(tmpRate, out rate);
                double maxUnit = 0;
                double.TryParse(tmpMaxUnit, out maxUnit);
                double minUnit = 0;
                double.TryParse(tmpMinUnit, out minUnit);

                SORDetailCreate cre = new SORDetailCreate(this, myConnection, SORDetailCreate.ScreenMode.Edit, mySorID, bandID, band, minUnit, maxUnit, rate);
                cre.MdiParent = Globals.MDIParentForm;
                cre.Show();
                cre.WindowState = FormWindowState.Normal;
                Globals.HideScreen(this);
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }

        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gridData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                PopulateGrid();
            }
        }
    }
}
