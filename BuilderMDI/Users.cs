﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BuilderMDI
{
    public partial class Users : Form
    {

        SqlConnection myConnection;
        DataTable dtSelected;
        Form myParent;

        public Users(Form p, SqlConnection cnn)
        {
            InitializeComponent();
            this.Icon = BuilderMDI.Properties.Resources.ICON;
            myConnection = cnn;
            myParent = p;
        }

        private void Users_Load(object sender, EventArgs e)
        {
            Form frm = sender as Form;
            SetScreen(frm);
        }

        private void SetScreen(Form frm)
        {
            if (frm.Width <= 400)
                frm.Width = 400;

            groupBox1.Left = 5;
            groupBox1.Top = 5;
            groupBox1.Width = frm.ClientSize.Width - groupBox1.Left - 5;
            groupBox1.Height = frm.ClientSize.Height- groupBox1.Top - btNew.ClientSize.Height - 10;

            gridData.Left = 5;
            gridData.Top = 12;

            gridData.Width = groupBox1.Width - (gridData.Left * 2);
            gridData.Height = groupBox1.Height - gridData.Top - 10;

            btNew.Left = 5;
            btNew.Top = frm.ClientSize.Height - btNew.ClientSize.Height - 5;
            btDelete.Left = btNew.Left + btNew.Width + 10;
            btDelete.Top = btNew.Top;
            btClose.Top = btNew.Top;
        }

        private void Users_Shown(object sender, EventArgs e)
        {
            PopulateGrid();
            SetButtonStates();
        }

        public void ForceRefresh()
        {
            PopulateGrid();
        }

        private void SetButtonStates()
        {
            btNew.Enabled = true;
            btDelete.Enabled = (gridData.SelectedRows.Count > 0);
        }

        private void PopulateGrid()
        {
            try
            {
                int row = -1;

                if (gridData.SelectedRows.Count == 1)
                    row = gridData.SelectedRows[0].Index;

                SqlCommand command = new SqlCommand("sp_Users_Select", myConnection);
                command.CommandType = CommandType.StoredProcedure;

                // populate the grid
                SqlDataReader reader = command.ExecuteReader();
                dtSelected = new DataTable();
                dtSelected.Load(reader);

                gridData.DataSource = new DataView(dtSelected);
                SetGridHeader();
                Globals.UpdateGridFont(gridData);

                // re-select the row
                if (row != -1)
                {
                    if (row > gridData.RowCount)
                        row = gridData.RowCount;

                    gridData.CurrentCell = gridData.Rows[row].Cells[0];
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }

        private void SetGridHeader()
        {
            try
            {
                gridData.Columns["Pin"].Visible = false;
                gridData.Columns["Id"].Visible = false;

                gridData.Columns["Name"].HeaderText = "User Name";
                gridData.Columns["Name"].Width = 150;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
            return;
        }

        private void Users_Resize(object sender, EventArgs e)
        {
            Form frm = sender as Form;
            SetScreen(frm);
        }

        private void Users_FormClosed(object sender, FormClosedEventArgs e)
        {
            //if (myParent.GetType() == typeof(Main))
            //{
            //    Globals.RestoreScreen(myParent);
            //}
        }

        private void btDelete_Click(object sender, EventArgs e)
        {
            if (DialogResult.No == MessageBox.Show("Are you sure?", "User Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
                return;

            if (gridData.SelectedRows.Count == 0)
                return;

            var row = gridData.SelectedRows[0].Index;

            String name = gridData.Rows[row].Cells["Name"].Value.ToString();
            int userId = (int)gridData.Rows[row].Cells["Id"].Value;

            if (Globals.UserName != null)
            {
                if (Globals.UserName.Trim() == name.Trim())
                {
                    MessageBox.Show("You are currently logged in with this user name. User can not be deleted.", "User Delete", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            if (DoDelete(userId) == 0)
                PopulateGrid();
        }

        private int DoDelete(int userId)
        {
            int retval = -1;

            try
            {
                // Set uyp the command object
                SqlCommand command = new SqlCommand("sp_Users_Delete", myConnection);
                command.CommandType = CommandType.StoredProcedure;

                // Add all parmeters including the output parameters here
                command.Parameters.Add(new SqlParameter("@Id", userId));

                SqlParameter parRetval = command.Parameters.Add("@retval", SqlDbType.Int);
                parRetval.Direction = ParameterDirection.ReturnValue;

                // Execute the SP
                command.ExecuteNonQuery();

                retval = (int)command.Parameters["@retval"].Value;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
                retval = -1;
            }

            return retval;
        }

        private void btNew_Click(object sender, EventArgs e)
        {
            UsersCreate frm = new UsersCreate(this, myConnection);
            frm.MdiParent = Globals.MDIParentForm;
            frm.Show();
            frm.WindowState = FormWindowState.Normal;
            Globals.HideScreen(this);
        }

        private void gridData_Click(object sender, EventArgs e)
        {
            SetButtonStates();
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gridData_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F5)
            {
                PopulateGrid();
            }
        }

    }
}
