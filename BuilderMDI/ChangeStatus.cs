﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BuilderMDI
{
    public partial class ChangeStatus : Form
    {
        SqlConnection myConnection;
        int WorkID;
        String Status;
        Form myParent;
        String Comments;

        public ChangeStatus(Form parent, SqlConnection cnn, int workId, String status, String comments)
        {
            InitializeComponent();
            this.Icon = BuilderMDI.Properties.Resources.ICON;
            myConnection = cnn;
            WorkID = workId;
            Status = status;
            myParent = parent;
            Comments = comments;
        }

        private void ChangeStatus_Load(object sender, EventArgs e)
        {
            PopulateListboxes();
            cbStatus.Text = Status;
            //tbComment.Text = Comments;
        }

        private void PopulateListboxes()
        {
            try
            {
                // populate the locaiton list
                SqlCommand locCommand = new SqlCommand("sp_Listbox_Status_Select", myConnection);
                locCommand.CommandType = CommandType.StoredProcedure;

                SqlDataReader reader = locCommand.ExecuteReader();
                DataTable locListData = new DataTable();
                locListData.Load(reader);

                cbStatus.DataSource = locListData;
                cbStatus.DisplayMember = "StatusDesc";
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }

        private void btSave_Click(object sender, EventArgs e)
        {
            int retval = -1;

            try
            {
                int statusId = 1;

                // It was populated with data table hence the selected value is a data row
                DataRowView data = (DataRowView)cbStatus.SelectedValue;

                // Retreive as string as this is the most 'generic' data type
                string tmpStatus = data.Row["StatusID"].ToString();

                //Convert to integer if possible
                if (!int.TryParse(tmpStatus, out statusId))
                {
                    MessageBox.Show("Failed to convert status");
                    return;
                }

                String comments = tbComment.Text;

                // Set up the command object
                SqlCommand command = new SqlCommand("sp_WorkHead_Update_Status", myConnection);
                command.CommandType = CommandType.StoredProcedure;

                // Add all parmeters including the output parameters here
                command.Parameters.Add(new SqlParameter("@WorkID", WorkID));
                command.Parameters.Add(new SqlParameter("@StatusID", statusId));
                command.Parameters.Add(new SqlParameter("@Comments", comments));

                SqlParameter parRetval = command.Parameters.Add("@retval", SqlDbType.Int);
                parRetval.Direction = ParameterDirection.ReturnValue;

                // Execute the SP
                command.ExecuteNonQuery();

                retval = (int)command.Parameters["@retval"].Value;
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
                retval = -1;
            }

            if (retval == 0)
            {
                //if (myParent.GetType() == typeof(Main))
                //{
                //    var m = myParent as Main;
                //    m.RestoreScreen(true);
                //}

                //myParent.ForceGridRefresh();
                this.Close();
            }


        }

        private void ChangeStatus_FormClosed(object sender, FormClosedEventArgs e)
        {
            Globals.RestoreScreen(myParent);

            if (myParent.GetType() == typeof(Main))
            {
                var m = myParent as Main;
                m.RestoreScreen(true);
            }
            else if (myParent.GetType() == typeof(WorkOrderDetail))
            {
                Globals.RestoreScreen(myParent);

                var m = myParent as WorkOrderDetail;
                m.ForceGridRefresh(true);
            }
            else
            {
                Globals.RestoreScreen(myParent);
            }
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
