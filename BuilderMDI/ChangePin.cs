﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BuilderMDI
{
    public partial class ChangePin : Form
    {
        SqlConnection myConnection;

        public ChangePin(SqlConnection cnn)
        {
            InitializeComponent();
            this.Icon = BuilderMDI.Properties.Resources.ICON;
            myConnection = cnn;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int oldPin = -1;
            if (!int.TryParse(tbOldPin.Text, out oldPin))
            {
                MessageBox.Show("Old PIN must be numeric", "Kinetic Work Orders", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            // Confirm old pin is ok.
            try
            {
                // Test login
                SqlCommand command = new SqlCommand("sp_User_CheckPin", myConnection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@Name", Globals.UserName));
                command.Parameters.Add(new SqlParameter("@Pin", oldPin));

                SqlParameter retval = command.Parameters.Add("@retval", SqlDbType.Int);
                retval.Direction = ParameterDirection.ReturnValue;
                command.ExecuteNonQuery();
                int spret = (int)command.Parameters["@retval"].Value;

                // If not success write error and exit
                if (spret != 0)
                {
                    MessageBox.Show("Old PIN is not correct", "Change PIN", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
                else
                {
                    // Confirm pins are different
                    if (tbOldPin.Text.Trim() == tbNewPin.Text.Trim())
                    {
                        MessageBox.Show("Old PIN is the same as the New PIN. PIN not changed.", "Change PIN", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }

                    // Confirm New pins match
                    if (tbConfirmPin.Text != tbNewPin.Text)
                    {
                        MessageBox.Show("Pin Confirmation does not match New PIN. PIN not changed.", "Change PIN", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }

                    // Confirm New pins match
                    if (tbNewPin.Text.Trim().Length < 4 || tbNewPin.Text.Trim().Length > 6)
                    {
                        MessageBox.Show("Please use a PIN that is between 4 and 6 numbers. PIN not changed.", "Change PIN", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }

                    int newPin = -1;
                    if (!int.TryParse(tbNewPin.Text, out newPin))
                    {
                        MessageBox.Show("New PIN must be numeric", "Change PIN", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }

                    if (DoChangePin(oldPin, newPin) == 0)
                    {
                        MessageBox.Show("PIN has been changed", "Change PIN", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }

                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }

        private int DoChangePin(int oldpin, int newpin)
        {
            int ret = 0;

            try
            {
                SqlCommand command = new SqlCommand("sp_User_ChangePin", myConnection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new SqlParameter("@Name", Globals.UserName));
                command.Parameters.Add(new SqlParameter("@NewPin", newpin));
                command.Parameters.Add(new SqlParameter("@OldPin", oldpin));

                SqlParameter retval = command.Parameters.Add("@retval", SqlDbType.Int);
                retval.Direction = ParameterDirection.ReturnValue;
                command.ExecuteNonQuery();
                int spret = (int)command.Parameters["@retval"].Value;
                if (spret != 0)
                {
                    ret = -1;
                }
                else
                {
                    ret = 0;
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
                ret = -1;
            }
            return ret;
        }

        private void ChangePin_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        protected override void OnShown(EventArgs e)
        {
            tbOldPin.Focus();
            base.OnShown(e);
        }

    }
}
